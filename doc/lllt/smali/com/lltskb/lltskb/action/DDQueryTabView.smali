.class public Lcom/lltskb/lltskb/action/DDQueryTabView;
.super Landroid/widget/LinearLayout;
.source "DDQueryTabView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DDQueryTabView"


# instance fields
.field private mIsInited:Z

.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 48
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mIsInited:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p1, 0x0

    .line 41
    iput-boolean p1, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mIsInited:Z

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/action/DDQueryTabView;Z)V
    .locals 0

    .line 39
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->initAccount(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/action/DDQueryTabView;)Landroid/content/BroadcastReceiver;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mReceiver:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/action/DDQueryTabView;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->changeAccount()V

    return-void
.end method

.method private changeAccount()V
    .locals 3

    .line 338
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->get()Lcom/lltskb/lltskb/engine/online/ModelFactory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/online/ModelFactory;->getUserQuery()Lcom/lltskb/lltskb/engine/online/IUserQuery;

    move-result-object v0

    const/4 v1, 0x0

    .line 339
    invoke-interface {v0, v1}, Lcom/lltskb/lltskb/engine/online/IUserQuery;->isSignedIn(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 343
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/UserProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 345
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 348
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->register()V

    .line 349
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 351
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 353
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void
.end method

.method private initAccount(Z)V
    .locals 3

    const v0, 0x7f090245

    .line 143
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_0

    return-void

    .line 146
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v1

    .line 147
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/UserMgr;->getLastUserInfo()Lcom/lltskb/lltskb/engine/online/dto/UserInfo;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 148
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/lltskb/lltskb/utils/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 149
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getUserName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/lltskb/lltskb/utils/StringUtils;->isNotEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 150
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getUserName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 152
    :cond_1
    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/online/dto/UserInfo;->getAccount()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    if-eqz p1, :cond_3

    const p1, 0x7f0d01f6

    goto :goto_0

    :cond_3
    const p1, 0x7f0d01e7

    .line 155
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void
.end method

.method private onAccount()V
    .locals 5

    const-string v0, "DDQueryTabView"

    const-string v1, "onAccount"

    .line 325
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0d01d5

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    .line 332
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;-><init>(Lcom/lltskb/lltskb/action/DDQueryTabView;)V

    .line 334
    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, ""

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Lcom/lltskb/lltskb/action/DDQueryTabView$CheckAccountAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private onBaoXian()V
    .locals 2

    .line 304
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://www.dainar.com/hkout/page/pingan/wap/ywx3/?a=169"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onBookTicket()V
    .locals 3

    .line 313
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/OrderTicketActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onCompleteOrder()V
    .locals 3

    .line 261
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/CompleteOrderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 262
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onFlight()V
    .locals 2

    .line 276
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1, v1, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onHeyan()V
    .locals 3

    .line 271
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/HeyanActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 272
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onHotel()V
    .locals 2

    .line 284
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const/4 v1, 0x0

    invoke-static {v0, v1, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onJieSongZhan()V
    .locals 2

    .line 249
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://m.ctrip.com/webapp/carch/chf/train?s=car&Allianceid=109045&sid=552847&popup=close&autoawaken=close"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onLife()V
    .locals 2

    .line 295
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://jump.luna.58.com/i/26QL"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onLineUpOrder()V
    .locals 3

    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/engine/online/view/HoubuOrderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onLottery()V
    .locals 2

    .line 288
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://wap.lltskb.com/9188/caipiao.html"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onMaShangJiaoChe()V
    .locals 2

    .line 253
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://m.ctrip.com/webapp/carch/rtn/index?s=car&isbk=0&Allianceid=109045&sid=552847&popup=close&autoawaken=close"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onMonitor()V
    .locals 3

    .line 240
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/MonitorTaskActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 241
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onNoCompleteOrder()V
    .locals 3

    .line 308
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/NoCompleteOrderActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 309
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onPassenger()V
    .locals 3

    .line 266
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/order/ContactActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 267
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private onQiChe()V
    .locals 3

    .line 299
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "https://h5.qichedaquan.com/?utm_source=lulutong"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "xingyuan"

    const-string v2, "\u70b9\u51fb"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private onQicheTicket()V
    .locals 2

    .line 245
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "https://h5.m.taobao.com/trip/car/search/index.html?ttid=12oap0000083"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onQunarOrder()V
    .locals 2

    .line 280
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://touch.qunar.com/h5/flight/flightorderqmc?bd_source=lulutong"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private onTourist()V
    .locals 2

    .line 257
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v1, "http://r.union.meituan.com/url/visit/?a=1&key=90f3f6fec96edbd161847183ca9ba6a4414&url=http%3A%2F%2Fi.meituan.com%2F%3Ftype_v3%3D162%26nodown"

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method private register()V
    .locals 3

    .line 362
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.lltskb.lltskb.order.login.result"

    .line 363
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 364
    new-instance v1, Lcom/lltskb/lltskb/action/DDQueryTabView$1;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/DDQueryTabView$1;-><init>(Lcom/lltskb/lltskb/action/DDQueryTabView;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 382
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->isValidContext(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    .line 385
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    .line 386
    iget-object v2, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    return-void
.end method

.method private startActivity(Landroid/content/Intent;)V
    .locals 1

    .line 358
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/content/Context;Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public initView()V
    .locals 3

    .line 67
    iget-boolean v0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mIsInited:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    .line 68
    iput-boolean v0, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mIsInited:Z

    .line 70
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0b0057

    const/4 v2, 0x0

    .line 72
    :try_start_0
    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const v0, 0x7f09010d

    .line 78
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090124

    .line 81
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 82
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090147

    .line 84
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090128

    .line 87
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 88
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09014a

    .line 90
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 91
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090136

    .line 93
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 94
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090143

    .line 96
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09013f

    .line 99
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 100
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/action/DDQueryTabView;->initAccount(Z)V

    return-void

    .line 74
    :catch_0
    iput-boolean v2, p0, Lcom/lltskb/lltskb/action/DDQueryTabView;->mIsInited:Z

    return-void
.end method

.method public onBack()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result p1

    const-string v0, "ddquery"

    sparse-switch p1, :sswitch_data_0

    goto/16 :goto_0

    .line 200
    :sswitch_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onTourist()V

    goto/16 :goto_0

    .line 179
    :sswitch_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onPassenger()V

    .line 180
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u5e38\u7528\u8054\u7cfb\u4eba"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 171
    :sswitch_2
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onNoCompleteOrder()V

    .line 172
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u672a\u5b8c\u6210\u8ba2\u5355"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :sswitch_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onMonitor()V

    .line 216
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u76d1\u63a7\u7ba1\u7406"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 203
    :sswitch_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onLottery()V

    goto :goto_0

    .line 187
    :sswitch_5
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onLineUpOrder()V

    .line 188
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u5019\u8865\u8ba2\u5355"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    :sswitch_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onHeyan()V

    .line 184
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u624b\u673a\u6838\u9a8c"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :sswitch_7
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onCompleteOrder()V

    .line 176
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u5df2\u5b8c\u6210\u8ba2\u5355"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :sswitch_8
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onBookTicket()V

    .line 168
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u5728\u7ebf\u8ba2\u7968"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 163
    :sswitch_9
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onAccount()V

    .line 164
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u5f53\u524d\u8d26\u6237"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 194
    :sswitch_a
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onHotel()V

    goto :goto_0

    .line 191
    :sswitch_b
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onFlight()V

    goto :goto_0

    .line 209
    :sswitch_c
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onQiChe()V

    goto :goto_0

    .line 212
    :sswitch_d
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onLife()V

    :goto_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090034 -> :sswitch_d
        0x7f090065 -> :sswitch_c
        0x7f0900d5 -> :sswitch_b
        0x7f0900f5 -> :sswitch_a
        0x7f09010d -> :sswitch_9
        0x7f090124 -> :sswitch_8
        0x7f090128 -> :sswitch_7
        0x7f090136 -> :sswitch_6
        0x7f09013f -> :sswitch_5
        0x7f090141 -> :sswitch_4
        0x7f090143 -> :sswitch_3
        0x7f090147 -> :sswitch_2
        0x7f09014a -> :sswitch_1
        0x7f090161 -> :sswitch_0
    .end sparse-switch
.end method
