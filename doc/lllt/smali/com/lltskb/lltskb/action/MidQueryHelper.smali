.class public Lcom/lltskb/lltskb/action/MidQueryHelper;
.super Ljava/lang/Object;
.source "MidQueryHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;,
        Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;,
        Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MidQueryHelper"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDate:Ljava/lang/String;

.field private mFrom:Ljava/lang/String;

.field private mFuzzy:Z

.field private mHidden:Z

.field private mListener:Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;

.field private mTo:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/lltskb/lltskb/action/MidQueryHelper$Listener;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mFrom:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mTo:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mDate:Ljava/lang/String;

    .line 41
    iput-boolean p5, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mFuzzy:Z

    .line 42
    iput-boolean p6, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mHidden:Z

    .line 43
    iput-object p7, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mListener:Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;

    .line 44
    iput-object p1, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->getSafeContext()Landroid/content/Context;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mFrom:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mTo:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/action/MidQueryHelper;Ljava/util/List;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/action/MidQueryHelper;->showSelectMidDialog(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z
    .locals 0

    .line 23
    iget-boolean p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mFuzzy:Z

    return p0
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/action/MidQueryHelper;)Z
    .locals 0

    .line 23
    iget-boolean p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mHidden:Z

    return p0
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/action/MidQueryHelper;)Ljava/lang/String;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mDate:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/action/MidQueryHelper;)Landroid/content/Context;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mContext:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/action/MidQueryHelper;)Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mListener:Lcom/lltskb/lltskb/action/MidQueryHelper$Listener;

    return-object p0
.end method

.method private doQueryM(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    const-string v0, "MidQueryHelper"

    const-string v1, "doQueryM"

    .line 93
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    new-instance v0, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;

    move-object v2, v0

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;-><init>(Lcom/lltskb/lltskb/action/MidQueryHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    sget-object p1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/String;

    const/4 p3, 0x0

    const-string p4, ""

    aput-object p4, p2, p3

    invoke-virtual {v0, p1, p2}, Lcom/lltskb/lltskb/action/MidQueryHelper$DoQueryMidTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private getSafeContext()Landroid/content/Context;
    .locals 1

    .line 48
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getCurrentContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private showSelectMidDialog(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/lltskb/lltskb/engine/ResultItem;",
            ">;)V"
        }
    .end annotation

    const-string v0, "MidQueryHelper"

    const-string v1, "showSelectMidDialog"

    .line 72
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/CharSequence;

    const/4 v2, 0x1

    .line 74
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 75
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/lltskb/lltskb/engine/ResultItem;

    add-int/lit8 v4, v2, -0x1

    .line 76
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mContext:Landroid/content/Context;

    const v6, 0x7f0d01b6

    .line 77
    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    :cond_0
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {p0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0d025f

    .line 81
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 82
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$K6NNyP-6dIJGELEO_Xoag-1l6nI;

    invoke-direct {v2, p0, p1}, Lcom/lltskb/lltskb/action/-$$Lambda$MidQueryHelper$K6NNyP-6dIJGELEO_Xoag-1l6nI;-><init>(Lcom/lltskb/lltskb/action/MidQueryHelper;Ljava/util/List;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 88
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 89
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$showSelectMidDialog$0$MidQueryHelper(Ljava/util/List;Landroid/content/DialogInterface;I)V
    .locals 1

    add-int/lit8 p3, p3, 0x1

    .line 83
    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/lltskb/lltskb/engine/ResultItem;

    const/4 p2, 0x0

    .line 84
    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/engine/ResultItem;->getText(I)Ljava/lang/String;

    move-result-object p1

    .line 86
    iget-object p2, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mFrom:Ljava/lang/String;

    iget-object p3, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mTo:Ljava/lang/String;

    iget-object v0, p0, Lcom/lltskb/lltskb/action/MidQueryHelper;->mDate:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p1, v0}, Lcom/lltskb/lltskb/action/MidQueryHelper;->doQueryM(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public searchMidStation()V
    .locals 5

    const-string v0, "MidQueryHelper"

    const-string v1, "doSearchMidStation"

    .line 59
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;-><init>(Lcom/lltskb/lltskb/action/MidQueryHelper;)V

    .line 63
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/action/MidQueryHelper$QueryMidTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
