.class public Lcom/lltskb/lltskb/action/StationListActivity;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "StationListActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "StationListActivity"


# instance fields
.field private mEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 5

    const-string v0, "StationListActivity"

    const-string v1, "initView"

    .line 45
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f090223

    .line 46
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/StationListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 48
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v2, 0x3

    const-string v3, "code"

    .line 50
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v3, 0x1

    const v4, 0x7f0d0283

    if-eq v1, v3, :cond_1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const v4, 0x7f0d0055

    goto :goto_0

    :cond_1
    const v4, 0x7f0d0280

    .line 63
    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    const v0, 0x7f0901db

    .line 66
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/action/StationListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    const v1, 0x7f0900ac

    .line 69
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/action/StationListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/lltskb/lltskb/action/StationListActivity;->mEditText:Landroid/widget/EditText;

    .line 70
    iget-object v1, p0, Lcom/lltskb/lltskb/action/StationListActivity;->mEditText:Landroid/widget/EditText;

    invoke-static {v1}, Lcom/lltskb/lltskb/utils/EditHelper;->installEditClear(Landroid/view/View;)V

    .line 71
    iget-object v1, p0, Lcom/lltskb/lltskb/action/StationListActivity;->mEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$4sPKbggaDrR3QvCN1VNNi3rEF2Q;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$4sPKbggaDrR3QvCN1VNNi3rEF2Q;-><init>(Lcom/lltskb/lltskb/action/StationListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 83
    new-instance v1, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;-><init>(Landroid/content/Context;Z)V

    .line 85
    new-instance v2, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$2djObXWZKIkD9MCyFGH4OluQPvA;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$2djObXWZKIkD9MCyFGH4OluQPvA;-><init>(Lcom/lltskb/lltskb/action/StationListActivity;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->setListener(Lcom/lltskb/lltskb/adapters/StationSelectAdapter$Listener;)V

    .line 93
    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    invoke-virtual {v1}, Lcom/lltskb/lltskb/adapters/StationSelectAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v2, p0, Lcom/lltskb/lltskb/action/StationListActivity;->mEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/lltskb/lltskb/action/StationListActivity$1;

    invoke-direct {v3, p0, v1}, Lcom/lltskb/lltskb/action/StationListActivity$1;-><init>(Lcom/lltskb/lltskb/action/StationListActivity;Lcom/lltskb/lltskb/adapters/StationSelectAdapter;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 117
    new-instance v1, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$KAr06XokocQeWy6SaeUStkq_cEE;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/-$$Lambda$StationListActivity$KAr06XokocQeWy6SaeUStkq_cEE;-><init>(Lcom/lltskb/lltskb/action/StationListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$initView$0$StationListActivity(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 73
    iget-object p1, p0, Lcom/lltskb/lltskb/action/StationListActivity;->mEditText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 74
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    .line 75
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string p3, "station"

    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/action/StationListActivity;->setResult(ILandroid/content/Intent;)V

    .line 77
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->finish()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$initView$1$StationListActivity(Ljava/lang/String;)V
    .locals 2

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onItemSelected = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StationListActivity"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "station"

    .line 88
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 89
    invoke-virtual {p0, p1, v0}, Lcom/lltskb/lltskb/action/StationListActivity;->setResult(ILandroid/content/Intent;)V

    .line 90
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->finish()V

    return-void
.end method

.method public synthetic lambda$initView$2$StationListActivity(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    const-string p2, "StationListActivity"

    const-string p4, "onItemClick "

    .line 118
    invoke-static {p2, p4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object p1

    invoke-interface {p1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 120
    new-instance p2, Landroid/content/Intent;

    invoke-direct {p2}, Landroid/content/Intent;-><init>()V

    const-string p3, "station"

    .line 121
    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 p1, -0x1

    .line 122
    invoke-virtual {p0, p1, p2}, Lcom/lltskb/lltskb/action/StationListActivity;->setResult(ILandroid/content/Intent;)V

    .line 123
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 38
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/Window;->requestFeature(I)Z

    const p1, 0x7f0b008c

    .line 40
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/action/StationListActivity;->setContentView(I)V

    .line 41
    invoke-direct {p0}, Lcom/lltskb/lltskb/action/StationListActivity;->initView()V

    return-void
.end method
