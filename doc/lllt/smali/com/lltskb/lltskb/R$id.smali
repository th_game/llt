.class public final Lcom/lltskb/lltskb/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lltskb/lltskb/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f090000

.field public static final CTRL:I = 0x7f090001

.field public static final ContentHScrollView:I = 0x7f090002

.field public static final FUNCTION:I = 0x7f090003

.field public static final ItemTextBeginEndTime:I = 0x7f090004

.field public static final ItemTextExtra:I = 0x7f090005

.field public static final ItemTextFuXing:I = 0x7f090006

.field public static final ItemTextPrice:I = 0x7f090007

.field public static final ItemTextStation:I = 0x7f090008

.field public static final ItemTextStationName:I = 0x7f090009

.field public static final ItemTextStopTime:I = 0x7f09000a

.field public static final ItemTextTicket:I = 0x7f09000b

.field public static final ItemTextTime:I = 0x7f09000c

.field public static final ItemTextTrainName:I = 0x7f09000d

.field public static final ItemTextType:I = 0x7f09000e

.field public static final Label_ArriveStation:I = 0x7f09000f

.field public static final Label_Date:I = 0x7f090010

.field public static final Label_StartStation:I = 0x7f090011

.field public static final Label_Type:I = 0x7f090012

.field public static final META:I = 0x7f090013

.field public static final SHIFT:I = 0x7f090014

.field public static final SYM:I = 0x7f090015

.field public static final TabLayout:I = 0x7f090016

.field public static final TextViewMsg:I = 0x7f090017

.field public static final TextViewVer:I = 0x7f090018

.field public static final Top_Date:I = 0x7f090019

.field public static final ViewPager:I = 0x7f09001a

.field public static final about:I = 0x7f09001b

.field public static final action0:I = 0x7f09001c

.field public static final action_bar:I = 0x7f09001d

.field public static final action_bar_activity_content:I = 0x7f09001e

.field public static final action_bar_container:I = 0x7f09001f

.field public static final action_bar_root:I = 0x7f090020

.field public static final action_bar_spinner:I = 0x7f090021

.field public static final action_bar_subtitle:I = 0x7f090022

.field public static final action_bar_title:I = 0x7f090023

.field public static final action_container:I = 0x7f090024

.field public static final action_context_bar:I = 0x7f090025

.field public static final action_divider:I = 0x7f090026

.field public static final action_image:I = 0x7f090027

.field public static final action_menu_divider:I = 0x7f090028

.field public static final action_menu_presenter:I = 0x7f090029

.field public static final action_mode_bar:I = 0x7f09002a

.field public static final action_mode_bar_stub:I = 0x7f09002b

.field public static final action_mode_close_button:I = 0x7f09002c

.field public static final action_text:I = 0x7f09002d

.field public static final actions:I = 0x7f09002e

.field public static final activity_chooser_view_content:I = 0x7f09002f

.field public static final ad_list:I = 0x7f090030

.field public static final add:I = 0x7f090031

.field public static final alertTitle:I = 0x7f090032

.field public static final all:I = 0x7f090033

.field public static final all_life_view:I = 0x7f090034

.field public static final always:I = 0x7f090035

.field public static final async:I = 0x7f090036

.field public static final auto:I = 0x7f090037

.field public static final baidu_splash:I = 0x7f090038

.field public static final baidu_view:I = 0x7f090039

.field public static final beginning:I = 0x7f09003a

.field public static final big_gridview:I = 0x7f09003b

.field public static final blocking:I = 0x7f09003c

.field public static final bottom:I = 0x7f09003d

.field public static final bottom_bar:I = 0x7f09003e

.field public static final btn_a:I = 0x7f09003f

.field public static final btn_action:I = 0x7f090040

.field public static final btn_add:I = 0x7f090041

.field public static final btn_add_user:I = 0x7f090042

.field public static final btn_b:I = 0x7f090043

.field public static final btn_back:I = 0x7f090044

.field public static final btn_bg_tasks:I = 0x7f090045

.field public static final btn_big_screen:I = 0x7f090046

.field public static final btn_book:I = 0x7f090047

.field public static final btn_c:I = 0x7f090048

.field public static final btn_cancel_order:I = 0x7f090049

.field public static final btn_close:I = 0x7f09004a

.field public static final btn_continue_pay:I = 0x7f09004b

.field public static final btn_d:I = 0x7f09004c

.field public static final btn_del:I = 0x7f09004d

.field public static final btn_direction:I = 0x7f09004e

.field public static final btn_down:I = 0x7f09004f

.field public static final btn_download:I = 0x7f090050

.field public static final btn_edit:I = 0x7f090051

.field public static final btn_f:I = 0x7f090052

.field public static final btn_feedback:I = 0x7f090053

.field public static final btn_getbx:I = 0x7f090054

.field public static final btn_heyan:I = 0x7f090055

.field public static final btn_homemaking:I = 0x7f090056

.field public static final btn_hotel:I = 0x7f090057

.field public static final btn_house:I = 0x7f090058

.field public static final btn_jlb:I = 0x7f090059

.field public static final btn_job:I = 0x7f09005a

.field public static final btn_layout:I = 0x7f09005b

.field public static final btn_login:I = 0x7f09005c

.field public static final btn_n:I = 0x7f09005d

.field public static final btn_next:I = 0x7f09005e

.field public static final btn_next_month:I = 0x7f09005f

.field public static final btn_no_limit:I = 0x7f090060

.field public static final btn_ok:I = 0x7f090061

.field public static final btn_order:I = 0x7f090062

.field public static final btn_p:I = 0x7f090063

.field public static final btn_pre_month:I = 0x7f090064

.field public static final btn_qiche:I = 0x7f090065

.field public static final btn_query:I = 0x7f090066

.field public static final btn_refresh:I = 0x7f090067

.field public static final btn_refresh_result:I = 0x7f090068

.field public static final btn_retry:I = 0x7f090069

.field public static final btn_returnTicket:I = 0x7f09006a

.field public static final btn_select_user:I = 0x7f09006b

.field public static final btn_set_filter:I = 0x7f09006c

.field public static final btn_set_mid:I = 0x7f09006d

.field public static final btn_set_sort:I = 0x7f09006e

.field public static final btn_share:I = 0x7f09006f

.field public static final btn_signout:I = 0x7f090070

.field public static final btn_station:I = 0x7f090071

.field public static final btn_stop:I = 0x7f090072

.field public static final btn_stu_order:I = 0x7f090073

.field public static final btn_submit:I = 0x7f090074

.field public static final btn_switch:I = 0x7f090075

.field public static final btn_syn:I = 0x7f090076

.field public static final btn_ticket:I = 0x7f090077

.field public static final btn_zwd:I = 0x7f090078

.field public static final buttonLayout:I = 0x7f090079

.field public static final buttonPanel:I = 0x7f09007a

.field public static final cancelBtn:I = 0x7f09007b

.field public static final cancelButton:I = 0x7f09007c

.field public static final cancel_action:I = 0x7f09007d

.field public static final center:I = 0x7f09007e

.field public static final center_horizontal:I = 0x7f09007f

.field public static final center_vertical:I = 0x7f090080

.field public static final checkbox:I = 0x7f090081

.field public static final chk_baike:I = 0x7f090082

.field public static final chk_bx_confirm:I = 0x7f090083

.field public static final chk_fuzzy:I = 0x7f090084

.field public static final chk_info:I = 0x7f090085

.field public static final chk_jpk:I = 0x7f090086

.field public static final chk_lagecy_line:I = 0x7f090087

.field public static final chk_online:I = 0x7f090088

.field public static final chk_qss:I = 0x7f090089

.field public static final chk_rem_name:I = 0x7f09008a

.field public static final chk_rem_pass:I = 0x7f09008b

.field public static final chk_select:I = 0x7f09008c

.field public static final chk_ticket:I = 0x7f09008d

.field public static final chk_train:I = 0x7f09008e

.field public static final chk_user:I = 0x7f09008f

.field public static final chk_zwd:I = 0x7f090090

.field public static final chronometer:I = 0x7f090091

.field public static final classui:I = 0x7f090092

.field public static final clip_horizontal:I = 0x7f090093

.field public static final clip_vertical:I = 0x7f090094

.field public static final collapseActionView:I = 0x7f090095

.field public static final collapsing_toolbar:I = 0x7f090096

.field public static final container:I = 0x7f090097

.field public static final contentPanel:I = 0x7f090098

.field public static final contentView:I = 0x7f090099

.field public static final content_layout:I = 0x7f09009a

.field public static final content_list:I = 0x7f09009b

.field public static final coordinator:I = 0x7f09009c

.field public static final custom:I = 0x7f09009d

.field public static final customPanel:I = 0x7f09009e

.field public static final date_picker:I = 0x7f09009f

.field public static final decor_content_parent:I = 0x7f0900a0

.field public static final default_activity_button:I = 0x7f0900a1

.field public static final default_query_date_value:I = 0x7f0900a2

.field public static final design_bottom_sheet:I = 0x7f0900a3

.field public static final design_menu_item_action_area:I = 0x7f0900a4

.field public static final design_menu_item_action_area_stub:I = 0x7f0900a5

.field public static final design_menu_item_text:I = 0x7f0900a6

.field public static final design_navigation_view:I = 0x7f0900a7

.field public static final disableHome:I = 0x7f0900a8

.field public static final duia_view:I = 0x7f0900a9

.field public static final edit_arrivestation:I = 0x7f0900aa

.field public static final edit_date:I = 0x7f0900ab

.field public static final edit_input:I = 0x7f0900ac

.field public static final edit_query:I = 0x7f0900ad

.field public static final edit_startstation:I = 0x7f0900ae

.field public static final edit_station:I = 0x7f0900af

.field public static final edit_train:I = 0x7f0900b0

.field public static final edit_type:I = 0x7f0900b1

.field public static final end:I = 0x7f0900b2

.field public static final end_padder:I = 0x7f0900b3

.field public static final enterAlways:I = 0x7f0900b4

.field public static final enterAlwaysCollapsed:I = 0x7f0900b5

.field public static final error_layout:I = 0x7f0900b6

.field public static final error_txt:I = 0x7f0900b7

.field public static final et_account:I = 0x7f0900b8

.field public static final et_condition:I = 0x7f0900b9

.field public static final et_confirmUserPass:I = 0x7f0900ba

.field public static final et_content:I = 0x7f0900bb

.field public static final et_email:I = 0x7f0900bc

.field public static final et_emailaddr:I = 0x7f0900bd

.field public static final et_end_qujian:I = 0x7f0900be

.field public static final et_id:I = 0x7f0900bf

.field public static final et_id_no:I = 0x7f0900c0

.field public static final et_mobile:I = 0x7f0900c1

.field public static final et_name:I = 0x7f0900c2

.field public static final et_new_mobile:I = 0x7f0900c3

.field public static final et_password:I = 0x7f0900c4

.field public static final et_phone:I = 0x7f0900c5

.field public static final et_school_name:I = 0x7f0900c6

.field public static final et_search:I = 0x7f0900c7

.field public static final et_start_qujian:I = 0x7f0900c8

.field public static final et_student_no:I = 0x7f0900c9

.field public static final et_subject:I = 0x7f0900ca

.field public static final et_userPass:I = 0x7f0900cb

.field public static final et_yzm:I = 0x7f0900cc

.field public static final exitUntilCollapsed:I = 0x7f0900cd

.field public static final expand_activities_button:I = 0x7f0900ce

.field public static final expanded_menu:I = 0x7f0900cf

.field public static final fill:I = 0x7f0900d0

.field public static final fill_horizontal:I = 0x7f0900d1

.field public static final fill_vertical:I = 0x7f0900d2

.field public static final first_seats:I = 0x7f0900d3

.field public static final fixed:I = 0x7f0900d4

.field public static final flight_view:I = 0x7f0900d5

.field public static final focusableLayout:I = 0x7f0900d6

.field public static final font_SeekBar:I = 0x7f0900d7

.field public static final font_size:I = 0x7f0900d8

.field public static final footer_arrow:I = 0x7f0900d9

.field public static final footer_hint_text:I = 0x7f0900da

.field public static final footer_layout:I = 0x7f0900db

.field public static final footer_progressbar:I = 0x7f0900dc

.field public static final forever:I = 0x7f0900dd

.field public static final fragment_container:I = 0x7f0900de

.field public static final fragment_detail_container:I = 0x7f0900df

.field public static final fragment_layout:I = 0x7f0900e0

.field public static final full_fragment_layout:I = 0x7f0900e1

.field public static final full_fragment_layout2:I = 0x7f0900e2

.field public static final fuzzy:I = 0x7f0900e3

.field public static final ghost_view:I = 0x7f0900e4

.field public static final group_book_type:I = 0x7f0900e5

.field public static final group_sex:I = 0x7f0900e6

.field public static final handle:I = 0x7f0900e7

.field public static final head:I = 0x7f0900e8

.field public static final header_arrow:I = 0x7f0900e9

.field public static final header_content:I = 0x7f0900ea

.field public static final header_hint_text:I = 0x7f0900eb

.field public static final header_hint_time:I = 0x7f0900ec

.field public static final header_layout:I = 0x7f0900ed

.field public static final header_progressbar:I = 0x7f0900ee

.field public static final header_text_layout:I = 0x7f0900ef

.field public static final hide:I = 0x7f0900f0

.field public static final hint_text:I = 0x7f0900f1

.field public static final histroy_view:I = 0x7f0900f2

.field public static final home:I = 0x7f0900f3

.field public static final homeAsUp:I = 0x7f0900f4

.field public static final hotel_view:I = 0x7f0900f5

.field public static final icon:I = 0x7f0900f6

.field public static final icon_group:I = 0x7f0900f7

.field public static final ifRoom:I = 0x7f0900f8

.field public static final image:I = 0x7f0900f9

.field public static final img_arrow:I = 0x7f0900fa

.field public static final img_back:I = 0x7f0900fb

.field public static final img_cc:I = 0x7f0900fc

.field public static final img_clear:I = 0x7f0900fd

.field public static final img_cz:I = 0x7f0900fe

.field public static final img_flight:I = 0x7f0900ff

.field public static final img_lottery_act:I = 0x7f090100

.field public static final img_settings:I = 0x7f090101

.field public static final img_yd:I = 0x7f090102

.field public static final img_zz:I = 0x7f090103

.field public static final in_header:I = 0x7f090104

.field public static final in_train_info:I = 0x7f090105

.field public static final info:I = 0x7f090106

.field public static final italic:I = 0x7f090107

.field public static final item_station_alpha:I = 0x7f090108

.field public static final item_station_name:I = 0x7f090109

.field public static final item_touch_helper_previous_elevation:I = 0x7f09010a

.field public static final jingzhungu_view:I = 0x7f09010b

.field public static final largeLabel:I = 0x7f09010c

.field public static final layout_account:I = 0x7f09010d

.field public static final layout_account_help:I = 0x7f09010e

.field public static final layout_ad:I = 0x7f09010f

.field public static final layout_adv:I = 0x7f090110

.field public static final layout_alert:I = 0x7f090111

.field public static final layout_all_life:I = 0x7f090112

.field public static final layout_arrive:I = 0x7f090113

.field public static final layout_baidu:I = 0x7f090114

.field public static final layout_bank_abc:I = 0x7f090115

.field public static final layout_bank_alipay:I = 0x7f090116

.field public static final layout_bank_alipay_client:I = 0x7f090117

.field public static final layout_bank_boc:I = 0x7f090118

.field public static final layout_bank_ccb:I = 0x7f090119

.field public static final layout_bank_cmb:I = 0x7f09011a

.field public static final layout_bank_icbc:I = 0x7f09011b

.field public static final layout_bank_other:I = 0x7f09011c

.field public static final layout_bank_union:I = 0x7f09011d

.field public static final layout_bank_weixin:I = 0x7f09011e

.field public static final layout_bank_ztyt:I = 0x7f09011f

.field public static final layout_baoxian:I = 0x7f090120

.field public static final layout_birthday:I = 0x7f090121

.field public static final layout_board_name:I = 0x7f090122

.field public static final layout_book_type:I = 0x7f090123

.field public static final layout_bookticket:I = 0x7f090124

.field public static final layout_bottom:I = 0x7f090125

.field public static final layout_btn:I = 0x7f090126

.field public static final layout_cc:I = 0x7f090127

.field public static final layout_complete_order:I = 0x7f090128

.field public static final layout_content:I = 0x7f090129

.field public static final layout_country:I = 0x7f09012a

.field public static final layout_date:I = 0x7f09012b

.field public static final layout_deadline:I = 0x7f09012c

.field public static final layout_direction:I = 0x7f09012d

.field public static final layout_ditie:I = 0x7f09012e

.field public static final layout_emailaddr:I = 0x7f09012f

.field public static final layout_enddate:I = 0x7f090130

.field public static final layout_enter_year:I = 0x7f090131

.field public static final layout_feedback:I = 0x7f090132

.field public static final layout_flight:I = 0x7f090133

.field public static final layout_frequence:I = 0x7f090134

.field public static final layout_head:I = 0x7f090135

.field public static final layout_heyan:I = 0x7f090136

.field public static final layout_homemaking:I = 0x7f090137

.field public static final layout_hotel:I = 0x7f090138

.field public static final layout_house:I = 0x7f090139

.field public static final layout_id_type:I = 0x7f09013a

.field public static final layout_info:I = 0x7f09013b

.field public static final layout_job:I = 0x7f09013c

.field public static final layout_jzg:I = 0x7f09013d

.field public static final layout_life:I = 0x7f09013e

.field public static final layout_lineup_order:I = 0x7f09013f

.field public static final layout_lltexam:I = 0x7f090140

.field public static final layout_lottery:I = 0x7f090141

.field public static final layout_main:I = 0x7f090142

.field public static final layout_monitor:I = 0x7f090143

.field public static final layout_more_ticket:I = 0x7f090144

.field public static final layout_mtlb:I = 0x7f090145

.field public static final layout_name:I = 0x7f090146

.field public static final layout_nocomplete_order:I = 0x7f090147

.field public static final layout_order_bottom:I = 0x7f090148

.field public static final layout_ordertype:I = 0x7f090149

.field public static final layout_passenger:I = 0x7f09014a

.field public static final layout_people:I = 0x7f09014b

.field public static final layout_price1:I = 0x7f09014c

.field public static final layout_price2:I = 0x7f09014d

.field public static final layout_province_code:I = 0x7f09014e

.field public static final layout_qiche:I = 0x7f09014f

.field public static final layout_qiche_ticket:I = 0x7f090150

.field public static final layout_query_date:I = 0x7f090151

.field public static final layout_qujian:I = 0x7f090152

.field public static final layout_qunar_order:I = 0x7f090153

.field public static final layout_runchart:I = 0x7f090154

.field public static final layout_school_system:I = 0x7f090155

.field public static final layout_seat:I = 0x7f090156

.field public static final layout_sex:I = 0x7f090157

.field public static final layout_start:I = 0x7f090158

.field public static final layout_startdate:I = 0x7f090159

.field public static final layout_station:I = 0x7f09015a

.field public static final layout_student:I = 0x7f09015b

.field public static final layout_subject:I = 0x7f09015c

.field public static final layout_ticket_type:I = 0x7f09015d

.field public static final layout_tielu:I = 0x7f09015e

.field public static final layout_time:I = 0x7f09015f

.field public static final layout_title:I = 0x7f090160

.field public static final layout_tourist:I = 0x7f090161

.field public static final layout_train:I = 0x7f090162

.field public static final layout_train_container:I = 0x7f090163

.field public static final layout_train_details:I = 0x7f090164

.field public static final layout_train_info:I = 0x7f090165

.field public static final layout_type:I = 0x7f090166

.field public static final layout_user_type:I = 0x7f090167

.field public static final layout_ver:I = 0x7f090168

.field public static final layout_web_browser:I = 0x7f090169

.field public static final layout_zc:I = 0x7f09016a

.field public static final layout_zwd_type:I = 0x7f09016b

.field public static final left:I = 0x7f09016c

.field public static final life_view:I = 0x7f09016d

.field public static final line1:I = 0x7f09016e

.field public static final line3:I = 0x7f09016f

.field public static final listMode:I = 0x7f090170

.field public static final list_item:I = 0x7f090171

.field public static final list_task:I = 0x7f090172

.field public static final list_train:I = 0x7f090173

.field public static final ll_houbu_order:I = 0x7f090174

.field public static final ll_run:I = 0x7f090175

.field public static final ll_show_runchart:I = 0x7f090176

.field public static final ll_station_search:I = 0x7f090177

.field public static final ll_stop:I = 0x7f090178

.field public static final lltskb_about:I = 0x7f090179

.field public static final lltskb_qrcode:I = 0x7f09017a

.field public static final lltskb_splash:I = 0x7f09017b

.field public static final loading_layout:I = 0x7f09017c

.field public static final loading_txt:I = 0x7f09017d

.field public static final lv_baoxian_order:I = 0x7f09017e

.field public static final lv_histroy:I = 0x7f09017f

.field public static final lv_list:I = 0x7f090180

.field public static final lv_passenger:I = 0x7f090181

.field public static final lv_result:I = 0x7f090182

.field public static final lv_seat:I = 0x7f090183

.field public static final lv_station:I = 0x7f090184

.field public static final lv_station_search:I = 0x7f090185

.field public static final lv_stations:I = 0x7f090186

.field public static final lv_ticket_order:I = 0x7f090187

.field public static final lv_tickets:I = 0x7f090188

.field public static final lv_train:I = 0x7f090189

.field public static final main_bottom:I = 0x7f09018a

.field public static final main_content:I = 0x7f09018b

.field public static final mainlltskb:I = 0x7f09018c

.field public static final masked:I = 0x7f09018d

.field public static final material_background:I = 0x7f09018e

.field public static final media_actions:I = 0x7f09018f

.field public static final message:I = 0x7f090190

.field public static final message_content_root:I = 0x7f090191

.field public static final message_content_view:I = 0x7f090192

.field public static final message_layout:I = 0x7f090193

.field public static final middle:I = 0x7f090194

.field public static final mini:I = 0x7f090195

.field public static final multiply:I = 0x7f090196

.field public static final navbar:I = 0x7f090197

.field public static final navigation_header_container:I = 0x7f090198

.field public static final never:I = 0x7f090199

.field public static final noBtn:I = 0x7f09019a

.field public static final none:I = 0x7f09019b

.field public static final normal:I = 0x7f09019c

.field public static final notification_background:I = 0x7f09019d

.field public static final notification_main_column:I = 0x7f09019e

.field public static final notification_main_column_container:I = 0x7f09019f

.field public static final okBtn:I = 0x7f0901a0

.field public static final pager_layout:I = 0x7f0901a1

.field public static final parallax:I = 0x7f0901a2

.field public static final parentPanel:I = 0x7f0901a3

.field public static final parent_matrix:I = 0x7f0901a4

.field public static final passcode_edt:I = 0x7f0901a5

.field public static final passcode_img:I = 0x7f0901a6

.field public static final pb_loading:I = 0x7f0901a7

.field public static final pb_monitor:I = 0x7f0901a8

.field public static final pin:I = 0x7f0901a9

.field public static final prog_bar:I = 0x7f0901aa

.field public static final prog_loading:I = 0x7f0901ab

.field public static final progbar:I = 0x7f0901ac

.field public static final progress_circular:I = 0x7f0901ad

.field public static final progress_horizontal:I = 0x7f0901ae

.field public static final qiche_view:I = 0x7f0901af

.field public static final querycc_btn:I = 0x7f0901b0

.field public static final querycz_btn:I = 0x7f0901b1

.field public static final radio:I = 0x7f0901b2

.field public static final rb_direction_all:I = 0x7f0901b3

.field public static final rb_direction_down:I = 0x7f0901b4

.field public static final rb_direction_end:I = 0x7f0901b5

.field public static final rb_direction_up:I = 0x7f0901b6

.field public static final rd_book_adult:I = 0x7f0901b7

.field public static final rd_book_student:I = 0x7f0901b8

.field public static final rd_order_time:I = 0x7f0901b9

.field public static final rd_order_type_g:I = 0x7f0901ba

.field public static final rd_order_type_h:I = 0x7f0901bb

.field public static final rd_sex_f:I = 0x7f0901bc

.field public static final rd_sex_m:I = 0x7f0901bd

.field public static final rd_take_time:I = 0x7f0901be

.field public static final recycler_view:I = 0x7f0901bf

.field public static final relativeLayout1:I = 0x7f0901c0

.field public static final result_info:I = 0x7f0901c1

.field public static final result_title:I = 0x7f0901c2

.field public static final rg_direction:I = 0x7f0901c3

.field public static final rg_flag:I = 0x7f0901c4

.field public static final rg_type:I = 0x7f0901c5

.field public static final right:I = 0x7f0901c6

.field public static final right_icon:I = 0x7f0901c7

.field public static final right_side:I = 0x7f0901c8

.field public static final rl_item:I = 0x7f0901c9

.field public static final save_image_matrix:I = 0x7f0901ca

.field public static final save_non_transition_alpha:I = 0x7f0901cb

.field public static final save_scale_type:I = 0x7f0901cc

.field public static final sbl_houbu:I = 0x7f0901cd

.field public static final screen:I = 0x7f0901ce

.field public static final scroll:I = 0x7f0901cf

.field public static final scrollContainter:I = 0x7f0901d0

.field public static final scrollIndicatorDown:I = 0x7f0901d1

.field public static final scrollIndicatorUp:I = 0x7f0901d2

.field public static final scrollView:I = 0x7f0901d3

.field public static final scrollable:I = 0x7f0901d4

.field public static final search_badge:I = 0x7f0901d5

.field public static final search_bar:I = 0x7f0901d6

.field public static final search_button:I = 0x7f0901d7

.field public static final search_close_btn:I = 0x7f0901d8

.field public static final search_edit_frame:I = 0x7f0901d9

.field public static final search_go_btn:I = 0x7f0901da

.field public static final search_gridview:I = 0x7f0901db

.field public static final search_mag_icon:I = 0x7f0901dc

.field public static final search_plate:I = 0x7f0901dd

.field public static final search_src_text:I = 0x7f0901de

.field public static final search_voice_btn:I = 0x7f0901df

.field public static final second_seats:I = 0x7f0901e0

.field public static final select_all:I = 0x7f0901e1

.field public static final select_dialog_listview:I = 0x7f0901e2

.field public static final settings:I = 0x7f0901e3

.field public static final share:I = 0x7f0901e4

.field public static final shortcut:I = 0x7f0901e5

.field public static final showCustom:I = 0x7f0901e6

.field public static final showHome:I = 0x7f0901e7

.field public static final showTitle:I = 0x7f0901e8

.field public static final single_listview:I = 0x7f0901e9

.field public static final smallLabel:I = 0x7f0901ea

.field public static final snackbar_action:I = 0x7f0901eb

.field public static final snackbar_text:I = 0x7f0901ec

.field public static final snap:I = 0x7f0901ed

.field public static final soft_desc:I = 0x7f0901ee

.field public static final soft_icon:I = 0x7f0901ef

.field public static final soft_item:I = 0x7f0901f0

.field public static final soft_name:I = 0x7f0901f1

.field public static final soft_size:I = 0x7f0901f2

.field public static final space:I = 0x7f0901f3

.field public static final spacer:I = 0x7f0901f4

.field public static final splash_holder:I = 0x7f0901f5

.field public static final split0:I = 0x7f0901f6

.field public static final split01:I = 0x7f0901f7

.field public static final split02:I = 0x7f0901f8

.field public static final split03:I = 0x7f0901f9

.field public static final split1:I = 0x7f0901fa

.field public static final split2:I = 0x7f0901fb

.field public static final split_action_bar:I = 0x7f0901fc

.field public static final src_atop:I = 0x7f0901fd

.field public static final src_in:I = 0x7f0901fe

.field public static final src_over:I = 0x7f0901ff

.field public static final start:I = 0x7f090200

.field public static final station_dialog:I = 0x7f090201

.field public static final station_locationbar:I = 0x7f090202

.field public static final station_name:I = 0x7f090203

.field public static final status_bar_latest_event_content:I = 0x7f090204

.field public static final submenuarrow:I = 0x7f090205

.field public static final submitButton:I = 0x7f090206

.field public static final submit_area:I = 0x7f090207

.field public static final switch_classui:I = 0x7f090208

.field public static final switch_fuzzy:I = 0x7f090209

.field public static final switch_hide:I = 0x7f09020a

.field public static final switch_show_runchart:I = 0x7f09020b

.field public static final switch_station:I = 0x7f09020c

.field public static final switch_type:I = 0x7f09020d

.field public static final tabMode:I = 0x7f09020e

.field public static final tab_cc:I = 0x7f09020f

.field public static final tab_cz:I = 0x7f090210

.field public static final tab_settings:I = 0x7f090211

.field public static final tab_yd:I = 0x7f090212

.field public static final tab_zz:I = 0x7f090213

.field public static final tabpager:I = 0x7f090214

.field public static final tag_transition_group:I = 0x7f090215

.field public static final text:I = 0x7f090216

.field public static final text2:I = 0x7f090217

.field public static final textSpacerNoButtons:I = 0x7f090218

.field public static final textSpacerNoTitle:I = 0x7f090219

.field public static final textView:I = 0x7f09021a

.field public static final textView2:I = 0x7f09021b

.field public static final textView3:I = 0x7f09021c

.field public static final text_input_password_toggle:I = 0x7f09021d

.field public static final text_sample:I = 0x7f09021e

.field public static final textinput_counter:I = 0x7f09021f

.field public static final textinput_error:I = 0x7f090220

.field public static final time:I = 0x7f090221

.field public static final time_picker:I = 0x7f090222

.field public static final title:I = 0x7f090223

.field public static final titleDividerNoCustom:I = 0x7f090224

.field public static final title_layout:I = 0x7f090225

.field public static final title_template:I = 0x7f090226

.field public static final tl_bigscreen:I = 0x7f090227

.field public static final tl_order:I = 0x7f090228

.field public static final today:I = 0x7f090229

.field public static final toggle_pwd:I = 0x7f09022a

.field public static final toolbar:I = 0x7f09022b

.field public static final top:I = 0x7f09022c

.field public static final topPanel:I = 0x7f09022d

.field public static final touch_outside:I = 0x7f09022e

.field public static final train_chkAll:I = 0x7f09022f

.field public static final train_chkDC:I = 0x7f090230

.field public static final train_chkG:I = 0x7f090231

.field public static final train_chkK:I = 0x7f090232

.field public static final train_chkPK:I = 0x7f090233

.field public static final train_chkPKE:I = 0x7f090234

.field public static final train_chkT:I = 0x7f090235

.field public static final train_chkZ:I = 0x7f090236

.field public static final transition_current_scene:I = 0x7f090237

.field public static final transition_layout_save:I = 0x7f090238

.field public static final transition_position:I = 0x7f090239

.field public static final transition_scene_layoutid_cache:I = 0x7f09023a

.field public static final transition_transform:I = 0x7f09023b

.field public static final tv_AlertType:I = 0x7f09023c

.field public static final tv_ArriveStation:I = 0x7f09023d

.field public static final tv_OrderPeople:I = 0x7f09023e

.field public static final tv_SeatText:I = 0x7f09023f

.field public static final tv_StartDate:I = 0x7f090240

.field public static final tv_StartStation:I = 0x7f090241

.field public static final tv_StartTrainNo:I = 0x7f090242

.field public static final tv_TrainTime:I = 0x7f090243

.field public static final tv_TrainTypeText:I = 0x7f090244

.field public static final tv_account:I = 0x7f090245

.field public static final tv_account_label:I = 0x7f090246

.field public static final tv_account_name_label:I = 0x7f090247

.field public static final tv_account_name_value:I = 0x7f090248

.field public static final tv_action:I = 0x7f090249

.field public static final tv_add_child_ticket:I = 0x7f09024a

.field public static final tv_alipay:I = 0x7f09024b

.field public static final tv_alipay_client:I = 0x7f09024c

.field public static final tv_arrive_time:I = 0x7f09024d

.field public static final tv_b:I = 0x7f09024e

.field public static final tv_baidu:I = 0x7f09024f

.field public static final tv_bank_abc:I = 0x7f090250

.field public static final tv_bank_boc:I = 0x7f090251

.field public static final tv_bank_ccb:I = 0x7f090252

.field public static final tv_bank_cmb:I = 0x7f090253

.field public static final tv_bank_icbc:I = 0x7f090254

.field public static final tv_bank_other:I = 0x7f090255

.field public static final tv_bank_union:I = 0x7f090256

.field public static final tv_bank_ztyt:I = 0x7f090257

.field public static final tv_baoxian:I = 0x7f090258

.field public static final tv_birthday:I = 0x7f090259

.field public static final tv_birthday_label:I = 0x7f09025a

.field public static final tv_birthday_value:I = 0x7f09025b

.field public static final tv_board_name:I = 0x7f09025c

.field public static final tv_book:I = 0x7f09025d

.field public static final tv_build_date:I = 0x7f09025e

.field public static final tv_build_date_value:I = 0x7f09025f

.field public static final tv_bx_confirm:I = 0x7f090260

.field public static final tv_cancel:I = 0x7f090261

.field public static final tv_cc:I = 0x7f090262

.field public static final tv_checkstatus_value:I = 0x7f090263

.field public static final tv_choosed_seats:I = 0x7f090264

.field public static final tv_coach:I = 0x7f090265

.field public static final tv_complete_order:I = 0x7f090266

.field public static final tv_confirmUserPass:I = 0x7f090267

.field public static final tv_content:I = 0x7f090268

.field public static final tv_content_label:I = 0x7f090269

.field public static final tv_copy_order:I = 0x7f09026a

.field public static final tv_count:I = 0x7f09026b

.field public static final tv_country:I = 0x7f09026c

.field public static final tv_country_label:I = 0x7f09026d

.field public static final tv_country_value:I = 0x7f09026e

.field public static final tv_cz:I = 0x7f09026f

.field public static final tv_d:I = 0x7f090270

.field public static final tv_data_ver:I = 0x7f090271

.field public static final tv_data_ver_value:I = 0x7f090272

.field public static final tv_date:I = 0x7f090273

.field public static final tv_date1:I = 0x7f090274

.field public static final tv_date2:I = 0x7f090275

.field public static final tv_deadline_time:I = 0x7f090276

.field public static final tv_desc:I = 0x7f090277

.field public static final tv_direction:I = 0x7f090278

.field public static final tv_ditie:I = 0x7f090279

.field public static final tv_duration:I = 0x7f09027a

.field public static final tv_email:I = 0x7f09027b

.field public static final tv_email_checkstatus_value:I = 0x7f09027c

.field public static final tv_email_value:I = 0x7f09027d

.field public static final tv_emailaddr_label:I = 0x7f09027e

.field public static final tv_end_date:I = 0x7f09027f

.field public static final tv_end_qujian_label:I = 0x7f090280

.field public static final tv_enddate_label:I = 0x7f090281

.field public static final tv_enter_year:I = 0x7f090282

.field public static final tv_enter_year_label:I = 0x7f090283

.field public static final tv_faq:I = 0x7f090284

.field public static final tv_flight:I = 0x7f090285

.field public static final tv_forget_pass:I = 0x7f090286

.field public static final tv_from:I = 0x7f090287

.field public static final tv_from_station_name:I = 0x7f090288

.field public static final tv_heyan:I = 0x7f090289

.field public static final tv_homemaking:I = 0x7f09028a

.field public static final tv_hotel:I = 0x7f09028b

.field public static final tv_house:I = 0x7f09028c

.field public static final tv_huoc_section:I = 0x7f09028d

.field public static final tv_id:I = 0x7f09028e

.field public static final tv_id_label:I = 0x7f09028f

.field public static final tv_id_type:I = 0x7f090290

.field public static final tv_id_type_label:I = 0x7f090291

.field public static final tv_id_value:I = 0x7f090292

.field public static final tv_info:I = 0x7f090293

.field public static final tv_job:I = 0x7f090294

.field public static final tv_jzg:I = 0x7f090295

.field public static final tv_label:I = 0x7f090296

.field public static final tv_left_ticket_hint:I = 0x7f090297

.field public static final tv_lib_date:I = 0x7f090298

.field public static final tv_lib_date_value:I = 0x7f090299

.field public static final tv_life:I = 0x7f09029a

.field public static final tv_life_section:I = 0x7f09029b

.field public static final tv_lineup_order:I = 0x7f09029c

.field public static final tv_lltexam:I = 0x7f09029d

.field public static final tv_login_tips:I = 0x7f09029e

.field public static final tv_lottery:I = 0x7f09029f

.field public static final tv_message:I = 0x7f0902a0

.field public static final tv_mobile:I = 0x7f0902a1

.field public static final tv_mobile_status:I = 0x7f0902a2

.field public static final tv_monitor:I = 0x7f0902a3

.field public static final tv_month:I = 0x7f0902a4

.field public static final tv_more:I = 0x7f0902a5

.field public static final tv_msg:I = 0x7f0902a6

.field public static final tv_mtlb:I = 0x7f0902a7

.field public static final tv_name:I = 0x7f0902a8

.field public static final tv_name_label:I = 0x7f0902a9

.field public static final tv_name_value:I = 0x7f0902aa

.field public static final tv_next_day:I = 0x7f0902ab

.field public static final tv_no:I = 0x7f0902ac

.field public static final tv_nocomplete_order:I = 0x7f0902ad

.field public static final tv_ok:I = 0x7f0902ae

.field public static final tv_online_heyan:I = 0x7f0902af

.field public static final tv_online_login:I = 0x7f0902b0

.field public static final tv_order_date:I = 0x7f0902b1

.field public static final tv_order_date_label:I = 0x7f0902b2

.field public static final tv_order_desc_no:I = 0x7f0902b3

.field public static final tv_order_desc_time:I = 0x7f0902b4

.field public static final tv_order_id:I = 0x7f0902b5

.field public static final tv_order_id_label:I = 0x7f0902b6

.field public static final tv_order_status:I = 0x7f0902b7

.field public static final tv_order_status_label:I = 0x7f0902b8

.field public static final tv_passenger:I = 0x7f0902b9

.field public static final tv_passenger_info:I = 0x7f0902ba

.field public static final tv_passer_name:I = 0x7f0902bb

.field public static final tv_phone:I = 0x7f0902bc

.field public static final tv_phone_checkstatus_value:I = 0x7f0902bd

.field public static final tv_phone_value:I = 0x7f0902be

.field public static final tv_prev_day:I = 0x7f0902bf

.field public static final tv_price:I = 0x7f0902c0

.field public static final tv_price1:I = 0x7f0902c1

.field public static final tv_price2:I = 0x7f0902c2

.field public static final tv_price_label:I = 0x7f0902c3

.field public static final tv_prog_ver:I = 0x7f0902c4

.field public static final tv_prog_ver_value:I = 0x7f0902c5

.field public static final tv_province_code:I = 0x7f0902c6

.field public static final tv_province_code_label:I = 0x7f0902c7

.field public static final tv_qiche:I = 0x7f0902c8

.field public static final tv_qiche_ticket:I = 0x7f0902c9

.field public static final tv_query_freq:I = 0x7f0902ca

.field public static final tv_qunar_order:I = 0x7f0902cb

.field public static final tv_record:I = 0x7f0902cc

.field public static final tv_red_point:I = 0x7f0902cd

.field public static final tv_refresh:I = 0x7f0902ce

.field public static final tv_register:I = 0x7f0902cf

.field public static final tv_return_resign:I = 0x7f0902d0

.field public static final tv_run_status:I = 0x7f0902d1

.field public static final tv_runchart:I = 0x7f0902d2

.field public static final tv_school_name_label:I = 0x7f0902d3

.field public static final tv_school_system:I = 0x7f0902d4

.field public static final tv_school_system_label:I = 0x7f0902d5

.field public static final tv_seat:I = 0x7f0902d6

.field public static final tv_seat_info:I = 0x7f0902d7

.field public static final tv_seat_name:I = 0x7f0902d8

.field public static final tv_seat_no:I = 0x7f0902d9

.field public static final tv_seat_num:I = 0x7f0902da

.field public static final tv_seat_type:I = 0x7f0902db

.field public static final tv_section_histroy:I = 0x7f0902dc

.field public static final tv_section_name:I = 0x7f0902dd

.field public static final tv_settings:I = 0x7f0902de

.field public static final tv_sex_label:I = 0x7f0902df

.field public static final tv_sex_value:I = 0x7f0902e0

.field public static final tv_share_order:I = 0x7f0902e1

.field public static final tv_sign:I = 0x7f0902e2

.field public static final tv_start_date:I = 0x7f0902e3

.field public static final tv_start_qujian_label:I = 0x7f0902e4

.field public static final tv_start_time:I = 0x7f0902e5

.field public static final tv_start_time_label:I = 0x7f0902e6

.field public static final tv_startdate_label:I = 0x7f0902e7

.field public static final tv_station_label:I = 0x7f0902e8

.field public static final tv_station_name:I = 0x7f0902e9

.field public static final tv_status:I = 0x7f0902ea

.field public static final tv_student_no_label:I = 0x7f0902eb

.field public static final tv_subject_label:I = 0x7f0902ec

.field public static final tv_ticket_count:I = 0x7f0902ed

.field public static final tv_ticket_num:I = 0x7f0902ee

.field public static final tv_ticket_num_label:I = 0x7f0902ef

.field public static final tv_ticket_price:I = 0x7f0902f0

.field public static final tv_ticket_status:I = 0x7f0902f1

.field public static final tv_ticket_type:I = 0x7f0902f2

.field public static final tv_ticket_type_label:I = 0x7f0902f3

.field public static final tv_tickets:I = 0x7f0902f4

.field public static final tv_tielu:I = 0x7f0902f5

.field public static final tv_title:I = 0x7f0902f6

.field public static final tv_to:I = 0x7f0902f7

.field public static final tv_to_station_name:I = 0x7f0902f8

.field public static final tv_tourist:I = 0x7f0902f9

.field public static final tv_train:I = 0x7f0902fa

.field public static final tv_train_code:I = 0x7f0902fb

.field public static final tv_train_info:I = 0x7f0902fc

.field public static final tv_train_info_label:I = 0x7f0902fd

.field public static final tv_train_name:I = 0x7f0902fe

.field public static final tv_train_name_label:I = 0x7f0902ff

.field public static final tv_type_name:I = 0x7f090300

.field public static final tv_userPass:I = 0x7f090301

.field public static final tv_user_type_label:I = 0x7f090302

.field public static final tv_user_type_value:I = 0x7f090303

.field public static final tv_username:I = 0x7f090304

.field public static final tv_version:I = 0x7f090305

.field public static final tv_view_mobile:I = 0x7f090306

.field public static final tv_weixin:I = 0x7f090307

.field public static final tv_yd:I = 0x7f090308

.field public static final tv_yes:I = 0x7f090309

.field public static final tv_zwd:I = 0x7f09030a

.field public static final tv_zwd_type:I = 0x7f09030b

.field public static final tv_zz:I = 0x7f09030c

.field public static final uniform:I = 0x7f09030d

.field public static final up:I = 0x7f09030e

.field public static final update:I = 0x7f09030f

.field public static final update_layout:I = 0x7f090310

.field public static final useLogo:I = 0x7f090311

.field public static final v_split:I = 0x7f090312

.field public static final version:I = 0x7f090313

.field public static final view_offset_helper:I = 0x7f090314

.field public static final view_temp:I = 0x7f090315

.field public static final view_temp2:I = 0x7f090316

.field public static final view_temp3:I = 0x7f090317

.field public static final visible:I = 0x7f090318

.field public static final wb_progbar:I = 0x7f090319

.field public static final webview:I = 0x7f09031a

.field public static final withText:I = 0x7f09031b

.field public static final wrap_content:I = 0x7f09031c

.field public static final yesBtn:I = 0x7f09031d

.field public static final zz_option:I = 0x7f09031e

.field public static final zzqueryform:I = 0x7f09031f


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
