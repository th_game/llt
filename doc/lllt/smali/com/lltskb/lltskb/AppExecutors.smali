.class public Lcom/lltskb/lltskb/AppExecutors;
.super Ljava/lang/Object;
.source "AppExecutors.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/AppExecutors$MainThreadExecutor;
    }
.end annotation


# instance fields
.field private final mDiskIO:Ljava/util/concurrent/Executor;

.field private final mMainThread:Ljava/util/concurrent/Executor;

.field private final mNetworkIO:Ljava/util/concurrent/Executor;


# direct methods
.method constructor <init>()V
    .locals 4

    .line 25
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/lltskb/lltskb/AppExecutors$MainThreadExecutor;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/lltskb/lltskb/AppExecutors$MainThreadExecutor;-><init>(Lcom/lltskb/lltskb/AppExecutors$1;)V

    invoke-direct {p0, v0, v1, v2}, Lcom/lltskb/lltskb/AppExecutors;-><init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/lltskb/lltskb/AppExecutors;->mDiskIO:Ljava/util/concurrent/Executor;

    .line 20
    iput-object p2, p0, Lcom/lltskb/lltskb/AppExecutors;->mNetworkIO:Ljava/util/concurrent/Executor;

    .line 21
    iput-object p3, p0, Lcom/lltskb/lltskb/AppExecutors;->mMainThread:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public diskIO()Ljava/util/concurrent/Executor;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/lltskb/lltskb/AppExecutors;->mDiskIO:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public mainThread()Ljava/util/concurrent/Executor;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/lltskb/lltskb/AppExecutors;->mMainThread:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public networkIO()Ljava/util/concurrent/Executor;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/lltskb/lltskb/AppExecutors;->mNetworkIO:Ljava/util/concurrent/Executor;

    return-object v0
.end method
