.class public Lcom/lltskb/lltskb/MainLLTSkb;
.super Lcom/lltskb/lltskb/BaseActivity;
.source "MainLLTSkb.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;,
        Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;,
        Lcom/lltskb/lltskb/MainLLTSkb$MyOnPageChangeListener;
    }
.end annotation


# static fields
.field public static final MENU_ABOUT_ID:I = 0x4

.field public static final MENU_EXIT_ID:I = 0x5

.field public static final MENU_HOME_ID:I = 0x2

.field public static final MENU_UPDATE_ID:I = 0x3

.field private static final SHOW_AD:I = 0x1

.field private static final SHOW_UPDATE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MainLLTSkb"

.field public static mShowAd:Z = true


# instance fields
.field private currTabIdx:I

.field private isSecondScreenDisplayed:Z

.field private mCCTabView:Lcom/lltskb/lltskb/action/CCQueryTabView;

.field private mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

.field private mDDTabView:Lcom/lltskb/lltskb/action/DDQueryTabView;

.field private mEnableSplashAd:Z

.field private mGDTSplashAd:Lcom/qq/e/ads/splash/SplashAD;

.field private mHandler:Landroid/os/Handler;

.field private mInAdSplash:Z

.field private mLastPressBack:Z

.field private mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

.field private mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

.field private mShowType:I

.field private mShowUpdateUI:Z

.field private mSkipCounter:I

.field private mSkipTimer:Ljava/util/Timer;

.field private mTabCC:Landroid/widget/ImageView;

.field private mTabCZ:Landroid/widget/ImageView;

.field private mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

.field private mTabSettings:Landroid/widget/ImageView;

.field private mTabYD:Landroid/widget/ImageView;

.field private mTabZZ:Landroid/widget/ImageView;

.field private mTextCC:Landroid/widget/TextView;

.field private mTextCZ:Landroid/widget/TextView;

.field private mTextSettings:Landroid/widget/TextView;

.field private mTextYD:Landroid/widget/TextView;

.field private mTextZZ:Landroid/widget/TextView;

.field private mTimeOutTimer:Ljava/util/Timer;

.field private mUpdateMgr:Lcom/lltskb/lltskb/engine/UpdateMgr;

.field private mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

.field private final threadGuard:Ljava/lang/Object;

.field private threadResult:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/BaseActivity;-><init>()V

    const/4 v0, 0x0

    .line 79
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    .line 82
    iput v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->threadResult:I

    .line 83
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->threadGuard:Ljava/lang/Object;

    const/4 v1, 0x3

    .line 88
    iput v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowType:I

    const/4 v1, 0x1

    .line 90
    iput-boolean v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    .line 91
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mInAdSplash:Z

    .line 95
    iput v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    .line 96
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mLastPressBack:Z

    .line 108
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->isSecondScreenDisplayed:Z

    .line 507
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x5

    .line 508
    iput v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    return-void
.end method

.method static synthetic access$000(Lcom/lltskb/lltskb/MainLLTSkb;)Landroid/os/Handler;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mHandler:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic access$100(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showBaiduSSPAd()V

    return-void
.end method

.method static synthetic access$1000(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/ZZQueryTabView;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->saveShowAd()V

    return-void
.end method

.method static synthetic access$1200(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->updateRedPoint()V

    return-void
.end method

.method static synthetic access$1300(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/engine/UpdateMgr;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mUpdateMgr:Lcom/lltskb/lltskb/engine/UpdateMgr;

    return-object p0
.end method

.method static synthetic access$1400(Lcom/lltskb/lltskb/MainLLTSkb;I)V
    .locals 0

    .line 73
    invoke-direct {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->onPageSelected(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->cleanFragment()V

    return-void
.end method

.method static synthetic access$1600(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/CustomViewPager;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    return-object p0
.end method

.method static synthetic access$1700(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/action/SettingsTabView;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    return-object p0
.end method

.method static synthetic access$1800(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showSplashAd()V

    return-void
.end method

.method static synthetic access$200(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->stopTimeoutTimer()V

    return-void
.end method

.method static synthetic access$300(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->startSkipTimer()V

    return-void
.end method

.method static synthetic access$400(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->checkPhoneStatePermission()V

    return-void
.end method

.method static synthetic access$500(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->checkStoragePermission()V

    return-void
.end method

.method static synthetic access$600(Lcom/lltskb/lltskb/MainLLTSkb;)V
    .locals 0

    .line 73
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->updateSkipButton()V

    return-void
.end method

.method static synthetic access$700(Lcom/lltskb/lltskb/MainLLTSkb;)Z
    .locals 0

    .line 73
    iget-boolean p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    return p0
.end method

.method static synthetic access$800(Lcom/lltskb/lltskb/MainLLTSkb;)I
    .locals 0

    .line 73
    iget p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->threadResult:I

    return p0
.end method

.method static synthetic access$900(Lcom/lltskb/lltskb/MainLLTSkb;)Lcom/lltskb/lltskb/view/LLTProgressDialog;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    return-object p0
.end method

.method private checkAdPermission()V
    .locals 2

    .line 490
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_0

    return-void

    .line 493
    :cond_0
    new-instance v0, Lcom/lltskb/lltskb/MainLLTSkb$5;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/MainLLTSkb$5;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    return-void
.end method

.method private checkPhoneStatePermission()V
    .locals 2

    .line 476
    new-instance v0, Lcom/lltskb/lltskb/MainLLTSkb$4;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/MainLLTSkb$4;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    return-void
.end method

.method private checkShowAd()V
    .locals 3

    const-string v0, "setting"

    const/4 v1, 0x0

    .line 1133
    invoke-virtual {p0, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1135
    :cond_0
    sget-boolean v1, Lcom/lltskb/lltskb/MainLLTSkb;->mShowAd:Z

    const-string v2, "showAd"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowAd:Z

    return-void
.end method

.method private checkStoragePermission()V
    .locals 2

    .line 462
    new-instance v0, Lcom/lltskb/lltskb/MainLLTSkb$3;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/MainLLTSkb$3;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v1, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    return-void
.end method

.method private cleanFragment()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "cleanFragment"

    .line 1284
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1285
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/fragment/AllLifeFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1286
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1287
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 1288
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1289
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 1292
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1293
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1294
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 1295
    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 1296
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method

.method private getSafeDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, " "

    .line 1540
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v1, 0x0

    .line 1542
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private initLeftTicket()V
    .locals 2

    .line 1139
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/lltskb/lltskb/MainLLTSkb$11;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/MainLLTSkb$11;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1149
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private onPageSelected(I)V
    .locals 6

    .line 1301
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onPageSelected index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainLLTSkb"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    const v0, 0x7f090225

    .line 1304
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    const v1, 0x7f090223

    .line 1308
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez v1, :cond_2

    return-void

    :cond_2
    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz p1, :cond_7

    if-eq p1, v4, :cond_6

    if-eq p1, v3, :cond_5

    const/4 v5, 0x3

    if-eq p1, v5, :cond_4

    if-eq p1, v2, :cond_3

    goto :goto_0

    .line 1328
    :cond_3
    iget-object v5, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/action/SettingsTabView;->initView()V

    const v5, 0x7f0d0268

    .line 1329
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    .line 1330
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->updateRedPoint()V

    goto :goto_0

    .line 1323
    :cond_4
    iget-object v5, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mDDTabView:Lcom/lltskb/lltskb/action/DDQueryTabView;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/action/DDQueryTabView;->initView()V

    const v5, 0x7f0d02e3

    .line 1324
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1319
    :cond_5
    iget-object v5, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/action/CZQueryTabView;->initView()V

    const v5, 0x7f0d00c5

    .line 1320
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1315
    :cond_6
    iget-object v5, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCCTabView:Lcom/lltskb/lltskb/action/CCQueryTabView;

    invoke-virtual {v5}, Lcom/lltskb/lltskb/action/CCQueryTabView;->initView()V

    const v5, 0x7f0d0095

    .line 1316
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_7
    const v5, 0x7f0d02ee

    .line 1312
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    const/4 v1, 0x0

    .line 1333
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1335
    invoke-direct {p0, p1, v4}, Lcom/lltskb/lltskb/MainLLTSkb;->setTabStatus(IZ)V

    .line 1336
    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    if-eq p1, v0, :cond_8

    .line 1337
    invoke-direct {p0, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->setTabStatus(IZ)V

    .line 1340
    :cond_8
    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    if-ne v0, v2, :cond_9

    .line 1341
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->saveSettings()V

    .line 1345
    :cond_9
    iput p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const p1, 0x7f090046

    .line 1347
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 1348
    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    if-ne v0, v3, :cond_a

    goto :goto_1

    :cond_a
    const/16 v1, 0x8

    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private prepareProg()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "prepareProg"

    .line 902
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 903
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    if-nez v0, :cond_0

    .line 904
    new-instance v0, Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const v1, 0x7f0e0143

    invoke-direct {v0, p0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    .line 905
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setCancelable(Z)V

    :cond_0
    return-void
.end method

.method private saveSettings()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "saveSettings"

    .line 1854
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const v1, 0x7f09020a

    .line 1857
    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_1

    return-void

    .line 1861
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setHideTrain(Z)V

    .line 1863
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const v1, 0x7f090209

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_2

    return-void

    .line 1866
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setFuzzyQuery(Z)V

    .line 1868
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const v1, 0x7f090208

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_3

    return-void

    .line 1871
    :cond_3
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setClassicStyle(Z)V

    .line 1873
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const v1, 0x7f09020b

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_4

    return-void

    .line 1875
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setShowRunChart(Z)V

    return-void
.end method

.method private saveShowAd()V
    .locals 3

    const-string v0, "setting"

    const/4 v1, 0x0

    .line 1123
    invoke-virtual {p0, v0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 1126
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const/4 v1, 0x1

    const-string v2, "showAd"

    .line 1127
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1128
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private setTabStatus(IZ)V
    .locals 1

    if-eqz p2, :cond_0

    const p2, 0x7f0600d0

    goto :goto_0

    :cond_0
    const p2, 0x7f0600cf

    .line 1353
    :goto_0
    invoke-static {p0, p2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getColor(Landroid/content/Context;I)I

    move-result p2

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    goto/16 :goto_1

    :cond_1
    const p1, 0x7f080140

    .line 1382
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/DialogUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1383
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1384
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabSettings:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1385
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextSettings:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_2
    const p1, 0x7f08013d

    .line 1376
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/DialogUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1377
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1378
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabYD:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1379
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextYD:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_3
    const p1, 0x7f08013f

    .line 1370
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/DialogUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1371
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1372
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCZ:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1373
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextCZ:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_4
    const p1, 0x7f08013e

    .line 1364
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/DialogUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1365
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1366
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCC:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1367
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextCC:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    :cond_5
    const p1, 0x7f080141

    .line 1358
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/DialogUtils;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1359
    invoke-static {p2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1360
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabZZ:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1361
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextZZ:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    return-void
.end method

.method private showAbout()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "showAbout"

    .line 889
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 890
    invoke-static {p0}, Lcom/lltskb/lltskb/view/AboutActivity;->start(Landroid/content/Context;)V

    return-void
.end method

.method private showBaiduSSPAd()V
    .locals 9

    const-string v0, "MainLLTSkb"

    const-string v1, "showBaiduSSPAd"

    .line 221
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0901f5

    .line 222
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    .line 226
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f09004a

    .line 227
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/16 v2, 0x8

    .line 228
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 229
    new-instance v2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$ouaV0BjBsGVU9qgn6eEelE2cwqE;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$ouaV0BjBsGVU9qgn6eEelE2cwqE;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    new-instance v6, Lcom/lltskb/lltskb/MainLLTSkb$2;

    invoke-direct {v6, p0}, Lcom/lltskb/lltskb/MainLLTSkb$2;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const/4 v1, 0x1

    .line 276
    iput-boolean v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mInAdSplash:Z

    .line 278
    invoke-static {v1}, Lcom/baidu/mobads/AdSettings;->setSupportHttps(Z)V

    .line 279
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getAppSid()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/baidu/mobads/AdView;->setAppSid(Landroid/content/Context;Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getPlaceId()Ljava/lang/String;

    move-result-object v7

    const v1, 0x7f090038

    .line 281
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/ViewGroup;

    .line 285
    new-instance v3, Lcom/baidu/mobads/SplashAd;

    const/4 v8, 0x1

    move-object v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/baidu/mobads/SplashAd;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/baidu/mobads/SplashAdListener;Ljava/lang/String;Z)V

    const-string v0, "baidussp"

    const-string v1, "\u8bf7\u6c42"

    .line 286
    invoke-static {p0, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private showGDTAd()V
    .locals 11

    const-string v0, "MainLLTSkb"

    const-string v1, "showGDT"

    .line 126
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x7f0901f5

    .line 127
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 129
    iget-boolean v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ConfigMgr;->shouldShowAd()Z

    move-result v2

    if-nez v2, :cond_0

    goto/16 :goto_0

    .line 137
    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_4

    const-string v2, "android.permission.READ_PHONE_STATE"

    .line 138
    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    const-string v4, "android.permission.ACCESS_FINE_LOCATION"

    const-string v5, "android.permission.WRITE_EXTERNAL_STORAGE"

    if-nez v3, :cond_1

    .line 139
    invoke-static {p0, v5}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 140
    invoke-static {p0, v4}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_2

    .line 141
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->checkAdPermission()V

    .line 144
    :cond_2
    invoke-static {p0, v2}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 145
    invoke-static {p0, v5}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    .line 146
    invoke-static {p0, v4}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_4

    .line 147
    :cond_3
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showBaiduSSPAd()V

    return-void

    :cond_4
    const/4 v2, 0x0

    .line 152
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v3, 0x7f09004a

    .line 153
    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object v6, v3

    check-cast v6, Landroid/widget/Button;

    .line 154
    invoke-virtual {v6, v2}, Landroid/widget/Button;->setVisibility(I)V

    const v2, 0x7f090038

    .line 155
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f09017b

    .line 157
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 159
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 161
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " height="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getScreenHeight(Landroid/content/Context;)I

    move-result v4

    div-int/lit8 v4, v4, 0x8

    if-le v0, v4, :cond_5

    .line 163
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getScreenHeight(Landroid/content/Context;)I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    iput v0, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 164
    invoke-virtual {v2, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    :cond_5
    new-instance v0, Lcom/qq/e/ads/splash/SplashAD;

    new-instance v9, Lcom/lltskb/lltskb/MainLLTSkb$1;

    invoke-direct {v9, p0, v6}, Lcom/lltskb/lltskb/MainLLTSkb$1;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Landroid/widget/Button;)V

    const/4 v10, 0x0

    const-string v7, "1107837297"

    const-string v8, "4060449291954141"

    move-object v4, v0

    move-object v5, p0

    invoke-direct/range {v4 .. v10}, Lcom/qq/e/ads/splash/SplashAD;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mGDTSplashAd:Lcom/qq/e/ads/splash/SplashAD;

    .line 216
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mGDTSplashAd:Lcom/qq/e/ads/splash/SplashAD;

    invoke-virtual {v0, v1}, Lcom/qq/e/ads/splash/SplashAD;->fetchAndShowIn(Landroid/view/ViewGroup;)V

    .line 217
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    const-string v1, "guangdiantong"

    const-string v2, "\u8bf7\u6c42"

    invoke-static {v0, v1, v2}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_6
    :goto_0
    if-eqz v1, :cond_7

    const/4 v0, 0x4

    .line 131
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 133
    :cond_7
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    return-void
.end method

.method private showSplashAd()V
    .locals 4

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "showSplashAd mEnableSplashA="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainLLTSkb"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0901f5

    .line 420
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 422
    iget-boolean v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/engine/ConfigMgr;->shouldShowAd()Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1

    .line 428
    :cond_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->startTimeoutTimer()V

    .line 429
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getNextAdType()I

    move-result v0

    .line 430
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ad_type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->shouldCheckPermissionTime()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 433
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->checkAdPermission()V

    .line 434
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setLastCheckPermissionTime()V

    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 441
    :try_start_0
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showGDTAd()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 443
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    .line 448
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showBaiduSSPAd()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 451
    :try_start_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showGDTAd()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    .line 453
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 456
    :cond_4
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showBaiduSSPAd()V

    :goto_0
    return-void

    :cond_5
    :goto_1
    const/4 v1, 0x4

    .line 423
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 424
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    return-void
.end method

.method private startSkipTimer()V
    .locals 7

    const/4 v0, 0x5

    .line 539
    iput v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    .line 540
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipTimer:Ljava/util/Timer;

    .line 541
    new-instance v2, Lcom/lltskb/lltskb/MainLLTSkb$7;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/MainLLTSkb$7;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 547
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipTimer:Ljava/util/Timer;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x3e8

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method private startTimeoutTimer()V
    .locals 4

    const-string v0, "MainLLTSkb"

    const-string v1, "startTimeoutTimer"

    .line 512
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTimeOutTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 517
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTimeOutTimer:Ljava/util/Timer;

    .line 518
    new-instance v0, Lcom/lltskb/lltskb/MainLLTSkb$6;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/MainLLTSkb$6;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 527
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTimeOutTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x4e20

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method

.method private stopSkipTimer()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "stopSkipTimer"

    .line 551
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 553
    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    const/4 v1, 0x0

    .line 554
    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipTimer:Ljava/util/Timer;

    .line 557
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    const v1, 0x7f09004a

    .line 559
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-eqz v1, :cond_3

    .line 560
    invoke-virtual {v1}, Landroid/widget/Button;->isShown()Z

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    const v0, 0x7f0901f5

    .line 565
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    const/4 v1, 0x4

    .line 567
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    :goto_0
    const-string v1, "close button not shown"

    .line 561
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private stopTimeoutTimer()V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "stopTimeoutTimer"

    .line 531
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTimeOutTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 533
    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    const/4 v0, 0x0

    .line 535
    iput-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTimeOutTimer:Ljava/util/Timer;

    return-void
.end method

.method private tryQuitApp()V
    .locals 4

    const-string v0, "MainLLTSkb"

    const-string v1, "tryQuitApp"

    .line 1227
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1228
    iget-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mLastPressBack:Z

    if-eqz v0, :cond_0

    .line 1229
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->finish()V

    goto :goto_0

    .line 1231
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0d0114

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1232
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$yydMI3Ihi55w6i9GXmN9_0W-GUc;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$yydMI3Ihi55w6i9GXmN9_0W-GUc;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    .line 1233
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mLastPressBack:Z

    :goto_0
    return-void
.end method

.method private updateRedPoint()V
    .locals 6

    .line 942
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/engine/LltSettings;->hasNewVersion()Z

    move-result v0

    .line 943
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateRedPoint isNew="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MainLLTSkb"

    invoke-static {v2, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const/4 v2, 0x0

    const/16 v3, 0x8

    const v4, 0x7f0902cd

    if-eqz v1, :cond_1

    .line 945
    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    const/16 v5, 0x8

    .line 947
    :goto_0
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const v1, 0x7f09018a

    .line 951
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 953
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    const/16 v2, 0x8

    .line 955
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    return-void
.end method

.method private updateSkipButton()V
    .locals 2

    .line 573
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 574
    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$fWZMzbVtlbFtuTaJ8w8tC4lpiSU;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$fWZMzbVtlbFtuTaJ8w8tC4lpiSU;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public btn_about(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_about"

    .line 1496
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1497
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showAbout()V

    .line 1498
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u5e2e\u52a9\u8bf4\u660e"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_all_life(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_all_life"

    .line 1449
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://wap.lltskb.com/shfw/"

    .line 1456
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1458
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u751f\u6d3b\u670d\u52a1"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_baidu(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_baidu"

    .line 1622
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1623
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1624
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "http://cpu.baidu.com/1032/1003cd5d"

    .line 1627
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1628
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "baidunews"

    const-string v1, "\u70b9\u51fb"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_default_date(Landroid/view/View;)V
    .locals 5

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_default_date"

    .line 1696
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/CharSequence;

    const v0, 0x7f0d00c4

    .line 1698
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const v0, 0x7f0d01dc

    .line 1699
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 1700
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const v2, 0x7f0900a2

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1702
    new-instance v2, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0d00cb

    .line 1703
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1713
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/LltSettings;->getDefaultQueryDate()I

    move-result v3

    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$13;

    invoke-direct {v4, p0, v0, p1}, Lcom/lltskb/lltskb/MainLLTSkb$13;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Landroid/widget/TextView;[Ljava/lang/CharSequence;)V

    invoke-virtual {v2, p1, v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x108000a

    .line 1722
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 1723
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1725
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 1728
    :try_start_0
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 1729
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_0

    const v0, 0x7f0e00bd

    .line 1731
    invoke-virtual {p1, v0}, Landroid/view/Window;->setWindowAnimations(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public btn_duia(Landroid/view/View;)V
    .locals 1

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_duia"

    .line 1588
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1590
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "https://engine.tuia.cn/index/activity?appKey=3ZoJy5pWvuzs9VkB4dTdQTZJa9sE&adslotId=1297"

    .line 1595
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public btn_feedback(Landroid/view/View;)V
    .locals 3

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_feedback"

    .line 1502
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    .line 1504
    invoke-static {}, Lcom/lltskb/lltskb/fragment/FeedBackFragment;->newInstance()Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 1505
    invoke-virtual {p1, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 1506
    const-class v1, Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0900e0

    invoke-virtual {p1, v2, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1509
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1510
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u610f\u89c1\u53cd\u9988"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_flight(Landroid/view/View;)V
    .locals 5

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_flight"

    .line 1548
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1549
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1550
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 1555
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    const/4 v0, 0x0

    if-eqz p1, :cond_7

    const v1, 0x7f0900aa

    .line 1556
    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_1

    .line 1557
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_1
    if-eqz p1, :cond_2

    .line 1558
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v0

    .line 1560
    :goto_0
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    const v2, 0x7f0900ae

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-nez v1, :cond_3

    .line 1561
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    :cond_3
    if-eqz v1, :cond_4

    .line 1562
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    .line 1564
    :goto_1
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    const v3, 0x7f0900ab

    invoke-virtual {v2, v3}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_5

    .line 1565
    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :cond_5
    if-eqz v2, :cond_6

    .line 1566
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_6
    move-object v2, v0

    .line 1567
    :goto_2
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->getSafeDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_7
    move-object p1, v0

    move-object v1, p1

    move-object v2, v1

    .line 1570
    :goto_3
    iget v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    move-object p1, v0

    goto :goto_4

    :cond_8
    move-object v0, v1

    .line 1575
    :goto_4
    invoke-static {p0, v0, p1, v2}, Lcom/lltskb/lltskb/utils/LLTUtils;->showFlight(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1576
    iget p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const-string v0, "\u673a\u7968"

    if-nez p1, :cond_9

    .line 1577
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "zzquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_9
    if-ne p1, v4, :cond_a

    .line 1579
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "ccquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_a
    const/4 v1, 0x2

    if-ne p1, v1, :cond_b

    .line 1581
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "czquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_b
    const/4 v1, 0x3

    if-ne p1, v1, :cond_c

    .line 1583
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "ddquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    :goto_5
    return-void
.end method

.method public btn_hotel(Landroid/view/View;)V
    .locals 5

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_hotel"

    .line 1632
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1634
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 1640
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    const/4 v0, 0x0

    const v1, 0x7f0900ab

    if-eqz p1, :cond_4

    const v2, 0x7f0900aa

    .line 1641
    invoke-virtual {p1, v2}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_1

    .line 1642
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_1
    if-eqz p1, :cond_2

    .line 1643
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    move-object p1, v0

    .line 1645
    :goto_0
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_3

    .line 1646
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :cond_3
    if-eqz v2, :cond_5

    .line 1647
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object p1, v0

    .line 1649
    :cond_5
    :goto_1
    iget v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 1650
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    if-eqz v2, :cond_a

    const v4, 0x7f0900af

    .line 1651
    invoke-virtual {v2, v4}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_6

    .line 1652
    invoke-virtual {p0, v4}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    :cond_6
    if-eqz v2, :cond_7

    .line 1653
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1655
    :cond_7
    iget-object v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    invoke-virtual {v2, v1}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-nez v2, :cond_8

    .line 1656
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/widget/TextView;

    :cond_8
    if-eqz v2, :cond_9

    .line 1657
    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1658
    :cond_9
    invoke-direct {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSafeDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1661
    :cond_a
    invoke-static {p0, p1, v0}, Lcom/lltskb/lltskb/utils/LLTUtils;->showHotel(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 1663
    iget p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const-string v0, "\u9152\u5e97"

    if-nez p1, :cond_b

    .line 1664
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "zzquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_b
    if-ne p1, v3, :cond_c

    .line 1666
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "ccquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    const/4 v1, 0x2

    if-ne p1, v1, :cond_d

    .line 1668
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "czquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_d
    const/4 v1, 0x3

    if-ne p1, v1, :cond_e

    .line 1670
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "ddquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_e
    :goto_2
    return-void
.end method

.method public btn_jzg(Landroid/view/View;)V
    .locals 1

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_jzg"

    .line 1599
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1600
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1601
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "http://m.jingzhengu.com/xiansuo/sellcar-lulutong.html"

    .line 1606
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public btn_life(Landroid/view/View;)V
    .locals 4

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_life"

    .line 1610
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1611
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    .line 1612
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 1616
    :cond_0
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    .line 1617
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/lltskb/lltskb/utils/LocationProvider;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v0

    const-string v0, "http://jump.luna.58.com/jump/sclk?url=UWd1sHcYnHEdnW9QPj9OPjDdsyd1symVUZR_IgwfUh0hm1d1pAR8uv6dUzun0jdkyMIunAGjuMPvyH6Eijb3UguQRMPvwbG1IbIhUNF5ngKpIduRpMwyUb6r0RKGIywFEyw3nZuQujKCNaYdXR7ZRy6xpjKnidmduvOyU-I7NRKJHMGzpbwu0hGByyOcH-uRuvOyINFbgY-ONywFEgD3IgG5ngKKNW6dIWubRgGdrZRONhFdHD_QPN6D&lat=%f&lon=%f"

    .line 1616
    invoke-static {p1, v0, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1618
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onShowRunChart(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onShowRunChart"

    .line 1831
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f09020b

    .line 1832
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_0

    .line 1834
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    :cond_0
    if-nez v0, :cond_1

    return-void

    .line 1837
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    .line 1838
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setShowRunChart(Z)V

    return-void
.end method

.method public btn_onclassui(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onclassui"

    .line 1400
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f090208

    .line 1401
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_0

    .line 1403
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    :cond_0
    if-nez v0, :cond_1

    return-void

    .line 1406
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    .line 1407
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/LltSettings;->setClassicStyle(Z)V

    .line 1408
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result p1

    const-string v0, "settings"

    if-eqz p1, :cond_2

    .line 1409
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u7ecf\u5178\u754c\u9762"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1411
    :cond_2
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "\u65b0\u754c\u9762"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public btn_onfont(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onfont"

    .line 1781
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1782
    new-instance p1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {p1, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1783
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    const v0, 0x7f0d015d

    .line 1785
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setTitle(I)V

    .line 1787
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 1789
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f0e00bd

    .line 1791
    invoke-virtual {v0, v1}, Landroid/view/Window;->setWindowAnimations(I)V

    :cond_0
    const v0, 0x7f0b0043

    .line 1794
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->setContentView(I)V

    const v0, 0x7f09021e

    .line 1796
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez v0, :cond_1

    return-void

    :cond_1
    const v1, 0x7f0900d7

    .line 1798
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/SeekBar;

    if-nez p1, :cond_2

    return-void

    :cond_2
    const/16 v1, 0x24

    .line 1800
    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 1801
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getFontSize()I

    move-result v1

    add-int/lit8 v1, v1, -0xa

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1802
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getFontSize()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1803
    new-instance v1, Lcom/lltskb/lltskb/MainLLTSkb$15;

    invoke-direct {v1, p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb$15;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Landroid/widget/TextView;)V

    invoke-virtual {p1, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1823
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u5b57\u4f53\u5927\u5c0f"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onfuzzy(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onfuzzy"

    .line 1416
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f090209

    .line 1417
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_0

    .line 1419
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    :cond_0
    if-nez v0, :cond_1

    return-void

    .line 1422
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    .line 1423
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setFuzzyQuery(Z)V

    .line 1424
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u6a21\u7cca\u7ad9\u540d"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onhide(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onhide"

    .line 1842
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f09020a

    .line 1843
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    if-nez v0, :cond_0

    .line 1845
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    move-object v0, p1

    check-cast v0, Lcom/lltskb/lltskb/view/SwitchButton;

    :cond_0
    if-nez v0, :cond_1

    return-void

    .line 1848
    :cond_1
    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/SwitchButton;->setChecked(Z)V

    .line 1849
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {v0}, Lcom/lltskb/lltskb/view/SwitchButton;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/engine/LltSettings;->setHideTrain(Z)V

    .line 1850
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u9690\u85cf\u8f66\u6b21"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onhome(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onhome"

    .line 1490
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const-string p1, "http://wap.lltskb.com"

    .line 1491
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1492
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u8bbf\u95ee\u4e3b\u9875"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onshare(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onshare"

    .line 1462
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1472
    new-instance p1, Lcom/lltskb/lltskb/MainLLTSkb$12;

    invoke-direct {p1, p0}, Lcom/lltskb/lltskb/MainLLTSkb$12;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-virtual {p0, v0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->requestPermission(Ljava/lang/String;Lcom/lltskb/lltskb/BaseActivity$IPermissionRequestCallback;)V

    .line 1485
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u5206\u4eab"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_onsoft(Landroid/view/View;)V
    .locals 3

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_onsoft"

    .line 1439
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object p1

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object p1

    const v0, 0x7f010018

    const v1, 0x7f010019

    .line 1441
    invoke-virtual {p1, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 1442
    invoke-static {}, Lcom/lltskb/lltskb/fragment/SoftFragment;->newInstance()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1443
    const-class v1, Lcom/lltskb/lltskb/fragment/SoftFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0900e0

    invoke-virtual {p1, v2, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1445
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method public btn_qiche(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_qiche"

    .line 1685
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1686
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1687
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "http://auto.news18a.com/m/price/lulutong/"

    .line 1691
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1692
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "qiche"

    const-string v1, "\u70b9\u51fb"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_qiche_ticket(Landroid/view/View;)V
    .locals 1

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_qiche_ticket"

    .line 1675
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    const/4 v0, 0x0

    .line 1677
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const-string p1, "https://h5.m.taobao.com/trip/car/search/index.html?ttid=12oap0000083"

    .line 1681
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    return-void
.end method

.method public btn_today_history(Landroid/view/View;)V
    .locals 6

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_today_history"

    .line 1514
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1517
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    const p1, 0x7f0d01d5

    .line 1518
    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    return-void

    .line 1522
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object p1

    .line 1523
    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x2

    .line 1524
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v4

    add-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    const-string p1, "http://baike.baidu.com/search/word?word=%d\u6708%d\u65e5&pic=1&sug=1&enc=utf8&oq=%d\u6708%d\u65e5"

    .line 1523
    invoke-static {v1, p1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1534
    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUtils;->showUrl(Landroid/app/Activity;Ljava/lang/String;)V

    .line 1535
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "settings"

    const-string v1, "\u5386\u53f2\u4e0a\u7684\u4eca\u5929"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public btn_update(Landroid/view/View;)V
    .locals 2

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_update"

    .line 1428
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 1429
    iput-boolean p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    .line 1430
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->update()V

    .line 1431
    iget p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    const-string v0, "\u68c0\u67e5\u66f4\u65b0"

    if-nez p1, :cond_0

    .line 1432
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "zzquery"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1434
    :cond_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v1, "settings"

    invoke-static {p1, v1, v0}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public btn_zwd_type(Landroid/view/View;)V
    .locals 5

    const-string p1, "MainLLTSkb"

    const-string v0, "btn_zwd_type"

    .line 1739
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/CharSequence;

    const v0, 0x7f0d02eb

    .line 1741
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    aput-object v0, p1, v1

    const v0, 0x7f0d02ec

    .line 1742
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 1743
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    const v2, 0x7f09030b

    invoke-virtual {v0, v2}, Lcom/lltskb/lltskb/action/SettingsTabView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1745
    new-instance v2, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0d02e9

    .line 1746
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1756
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/LltSettings;->getZwdQueryType()I

    move-result v3

    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$14;

    invoke-direct {v4, p0, v0, p1}, Lcom/lltskb/lltskb/MainLLTSkb$14;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Landroid/widget/TextView;[Ljava/lang/CharSequence;)V

    invoke-virtual {v2, p1, v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    const v0, 0x108000a

    .line 1765
    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setIcon(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object p1

    .line 1766
    invoke-virtual {p1, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 1767
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object p1

    .line 1770
    :try_start_0
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 1771
    invoke-virtual {p1}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    if-eqz p1, :cond_0

    const v0, 0x7f0e00bd

    .line 1773
    invoke-virtual {p1, v0}, Landroid/view/Window;->setWindowAnimations(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public doBigScreen(Landroid/view/View;)V
    .locals 3

    .line 2006
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    const v0, 0x7f0900af

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_0

    return-void

    .line 2010
    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2011
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/lltskb/lltskb/engine/online/view/BigScreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    const-string v2, "query_type"

    .line 2012
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "station"

    .line 2013
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2014
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 2015
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p1

    const-string v0, "czquery"

    const-string v1, "\u8f66\u7ad9\u5927\u5c4f"

    invoke-static {p1, v0, v1}, Lcom/baidu/mobstat/StatService;->onEvent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public doUpdate()V
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "doUpdate"

    .line 1000
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    new-instance v1, Lcom/lltskb/lltskb/engine/UpdateMgr;

    new-instance v2, Lcom/lltskb/lltskb/MainLLTSkb$10;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/MainLLTSkb$10;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-direct {v1, v2}, Lcom/lltskb/lltskb/engine/UpdateMgr;-><init>(Lcom/lltskb/lltskb/engine/IUpdateListener;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mUpdateMgr:Lcom/lltskb/lltskb/engine/UpdateMgr;

    .line 1113
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mUpdateMgr:Lcom/lltskb/lltskb/engine/UpdateMgr;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/UpdateMgr;->checkUpdate()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "checkUpdate failed"

    .line 1114
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1117
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public initMainUI()V
    .locals 7

    const-string v0, "MainLLTSkb"

    const-string v1, "initMainUI"

    .line 689
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 690
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->stopTimeoutTimer()V

    const v1, 0x7f090103

    .line 691
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v1, "initMainUI reenter"

    .line 692
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const v2, 0x7f0b0052

    .line 696
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->setContentView(I)V

    const v2, 0x7f0d0051

    .line 699
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 702
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->setTitle(Ljava/lang/CharSequence;)V

    .line 704
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabZZ:Landroid/widget/ImageView;

    const v1, 0x7f0900fc

    .line 705
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCC:Landroid/widget/ImageView;

    const v1, 0x7f0900fe

    .line 706
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCZ:Landroid/widget/ImageView;

    const v1, 0x7f090102

    .line 707
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabYD:Landroid/widget/ImageView;

    const v1, 0x7f090101

    .line 709
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabSettings:Landroid/widget/ImageView;

    .line 711
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabZZ:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const v3, 0x7f0600cf

    invoke-static {p0, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 712
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCC:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p0, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 713
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabCZ:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p0, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 714
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabYD:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p0, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 715
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabSettings:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {p0, v3}, Lcom/lltskb/lltskb/utils/DialogUtils;->getColor(Landroid/content/Context;I)I

    move-result v3

    invoke-static {v3}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->tintDrawable(Landroid/graphics/drawable/Drawable;Landroid/content/res/ColorStateList;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f09030c

    .line 717
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextZZ:Landroid/widget/TextView;

    const v1, 0x7f090262

    .line 718
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextCC:Landroid/widget/TextView;

    const v1, 0x7f09026f

    .line 719
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextCZ:Landroid/widget/TextView;

    const v1, 0x7f090308

    .line 720
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextYD:Landroid/widget/TextView;

    const v1, 0x7f0902de

    .line 721
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTextSettings:Landroid/widget/TextView;

    const v1, 0x7f090213

    .line 724
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    .line 725
    new-instance v3, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;

    invoke-direct {v3, p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v1, 0x7f09020f

    .line 726
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    .line 727
    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;

    invoke-direct {v4, p0, v3}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v1, 0x7f090210

    .line 728
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 729
    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;

    const/4 v5, 0x2

    invoke-direct {v4, p0, v5}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    const v1, 0x7f090212

    .line 730
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 731
    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;

    const/4 v5, 0x3

    invoke-direct {v4, p0, v5}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const v1, 0x7f090211

    .line 732
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 733
    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;

    const/4 v5, 0x4

    invoke-direct {v4, p0, v5}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnClickListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_5
    const v1, 0x7f090214

    .line 735
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/lltskb/lltskb/view/CustomViewPager;

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    .line 736
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    if-eqz v1, :cond_6

    .line 737
    new-instance v4, Lcom/lltskb/lltskb/MainLLTSkb$MyOnPageChangeListener;

    invoke-direct {v4, p0}, Lcom/lltskb/lltskb/MainLLTSkb$MyOnPageChangeListener;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {v1, v4}, Lcom/lltskb/lltskb/view/CustomViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 738
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/view/CustomViewPager;->setPagingEnabled(Z)V

    .line 741
    :cond_6
    new-instance v1, Lcom/lltskb/lltskb/action/ZZQueryTabView;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/ZZQueryTabView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    .line 742
    new-instance v1, Lcom/lltskb/lltskb/action/CCQueryTabView;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/CCQueryTabView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCCTabView:Lcom/lltskb/lltskb/action/CCQueryTabView;

    .line 743
    new-instance v1, Lcom/lltskb/lltskb/action/CZQueryTabView;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/CZQueryTabView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    .line 745
    new-instance v1, Lcom/lltskb/lltskb/action/DDQueryTabView;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/DDQueryTabView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mDDTabView:Lcom/lltskb/lltskb/action/DDQueryTabView;

    .line 746
    new-instance v1, Lcom/lltskb/lltskb/action/SettingsTabView;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/action/SettingsTabView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    .line 749
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 750
    iget-object v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    iget-object v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCCTabView:Lcom/lltskb/lltskb/action/CCQueryTabView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 752
    iget-object v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 754
    iget-object v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mDDTabView:Lcom/lltskb/lltskb/action/DDQueryTabView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    iget-object v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSettingsTabView:Lcom/lltskb/lltskb/action/SettingsTabView;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 758
    new-instance v3, Lcom/lltskb/lltskb/MainLLTSkb$8;

    invoke-direct {v3, p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb$8;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Ljava/util/ArrayList;)V

    .line 788
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/view/CustomViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 790
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    iget v3, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/view/CustomViewPager;->setCurrentItem(I)V

    .line 791
    iput-boolean v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mInAdSplash:Z

    .line 793
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    if-eqz v1, :cond_7

    const v3, 0x7f090313

    .line 794
    invoke-virtual {v1, v3}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz v1, :cond_7

    .line 796
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 802
    :cond_7
    invoke-static {p0, v2}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->showNewVersionComments(Landroid/content/Context;Z)Z

    move-result v1

    if-nez v1, :cond_9

    .line 804
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/LltSettings;->getLastUpdate()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/32 v5, 0x337f9800

    cmp-long v1, v3, v5

    if-gtz v1, :cond_8

    .line 805
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v1

    invoke-virtual {v1}, Lcom/lltskb/lltskb/engine/ResMgr;->getVer()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    .line 806
    :cond_8
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUtils;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 807
    iput-boolean v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    .line 808
    new-instance v1, Ljava/lang/Thread;

    new-instance v3, Lcom/lltskb/lltskb/MainLLTSkb$9;

    invoke-direct {v3, p0}, Lcom/lltskb/lltskb/MainLLTSkb$9;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-direct {v1, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 813
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 818
    :cond_9
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->updateRedPoint()V

    .line 820
    invoke-direct {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->onPageSelected(I)V

    const-string v1, "initMainUI end"

    .line 824
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public initResource()V
    .locals 5

    const-string v0, "MainLLTSkb"

    const-string v1, "initResource"

    .line 1153
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    new-instance v0, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    .line 1158
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/lltskb/lltskb/MainLLTSkb$InitResourceTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public synthetic lambda$null$3$MainLLTSkb(Landroid/view/View;)V
    .locals 0

    .line 982
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mUpdateMgr:Lcom/lltskb/lltskb/engine/UpdateMgr;

    if-eqz p1, :cond_0

    .line 983
    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/UpdateMgr;->stop()V

    .line 985
    :cond_0
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$showBaiduSSPAd$0$MainLLTSkb(Landroid/view/View;)V
    .locals 1

    const-string p1, "MainLLTSkb"

    const-string v0, "close the baidu splash"

    .line 230
    invoke-static {p1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const p1, 0x7f0901f5

    .line 231
    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    .line 233
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 234
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$showSelectDlg$4$MainLLTSkb(Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 2

    .line 976
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 977
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 978
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 979
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setIndeterminate(Z)V

    .line 980
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/16 v0, 0x64

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMax(I)V

    .line 981
    iget-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    new-instance v0, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$b2WkmDh4IA6fsgyydLEUBfVEcoE;

    invoke-direct {v0, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$b2WkmDh4IA6fsgyydLEUBfVEcoE;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {p2, v0}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setCancelBtnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p1, :cond_0

    .line 988
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object p2

    invoke-virtual {p2}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object p2

    invoke-virtual {p2}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object p2

    invoke-interface {p2, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$showSelectDlg$5$MainLLTSkb(Landroid/view/View;)V
    .locals 0

    .line 992
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$tryQuitApp$6$MainLLTSkb()V
    .locals 1

    const/4 v0, 0x0

    .line 1232
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mLastPressBack:Z

    return-void
.end method

.method public synthetic lambda$update$2$MainLLTSkb(Landroid/view/View;)V
    .locals 0

    .line 925
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$updateSkipButton$1$MainLLTSkb()V
    .locals 7

    .line 575
    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    if-nez v0, :cond_0

    .line 576
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->stopSkipTimer()V

    :cond_0
    const v0, 0x7f09004a

    .line 578
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    .line 580
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const v3, 0x7f0d0278

    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 581
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 583
    :cond_1
    iget v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mSkipCounter:I

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .line 1881
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onActivityResult requesetCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " resultCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainLLTSkb"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1883
    invoke-super {p0, p1, p2, p3}, Lcom/lltskb/lltskb/BaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    if-eqz p3, :cond_7

    .line 1885
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    goto/16 :goto_0

    .line 1886
    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "station"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_7

    .line 1888
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p3

    if-nez p3, :cond_1

    goto :goto_0

    :cond_1
    const/4 p3, 0x1

    if-ne p1, p3, :cond_3

    .line 1891
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    if-eqz p1, :cond_7

    const p3, 0x7f0900ae

    .line 1892
    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_2

    .line 1893
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_2
    if-eqz p1, :cond_7

    .line 1894
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    const/4 p3, 0x2

    if-ne p1, p3, :cond_5

    .line 1897
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mZZTabView:Lcom/lltskb/lltskb/action/ZZQueryTabView;

    if-eqz p1, :cond_7

    const p3, 0x7f0900aa

    .line 1898
    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/action/ZZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_4

    .line 1899
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_4
    if-eqz p1, :cond_7

    .line 1900
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    const/4 p3, 0x3

    if-ne p1, p3, :cond_7

    .line 1903
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mCZTabView:Lcom/lltskb/lltskb/action/CZQueryTabView;

    if-eqz p1, :cond_7

    const p3, 0x7f0900af

    .line 1904
    invoke-virtual {p1, p3}, Lcom/lltskb/lltskb/action/CZQueryTabView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    if-nez p1, :cond_6

    .line 1905
    invoke-virtual {p0, p3}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    :cond_6
    if-eqz p1, :cond_7

    .line 1906
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    :goto_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .line 1393
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onConfigurationChanged new orientation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainLLTSkb"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1394
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const-string v0, "MainLLTSkb"

    const-string v1, "onCreate"

    .line 592
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 595
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->setContext()V

    .line 596
    invoke-static {}, Lcom/lltskb/lltskb/engine/LltSettings;->get()Lcom/lltskb/lltskb/engine/LltSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/LltSettings;->load()V

    const-string p1, "java.util.Arrays.useLegacyMergeSort"

    const-string v1, "true"

    .line 598
    invoke-static {p1, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 601
    :try_start_0
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getCertificateSHA1Fingerprint(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 602
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->getDefaultSHA1()Ljava/lang/String;

    move-result-object v1

    .line 603
    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 604
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result p1

    invoke-static {p1}, Landroid/os/Process;->killProcess(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 607
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 612
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object p1

    iget p1, p1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 p1, p1, 0x2

    if-eqz p1, :cond_1

    .line 613
    invoke-static {}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->exit()V

    .line 617
    :cond_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object p1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->setContext(Landroid/content/Context;)V

    .line 619
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "config.json"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 621
    :try_start_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->init(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_2

    const-string p1, "init config.json failed"

    .line 622
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception p1

    .line 625
    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    :cond_2
    :goto_1
    invoke-static {}, Lcom/lltskb/lltskb/engine/ConfigMgr;->getInstance()Lcom/lltskb/lltskb/engine/ConfigMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ConfigMgr;->isEnableSSP()Z

    move-result p1

    iput-boolean p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    .line 630
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mEnableSplashAd="

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mEnableSplashAd:Z

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    invoke-static {p0}, Lcom/lltskb/lltskb/utils/WebViewUtil;->isWebViewCorrupted(Landroid/content/Context;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "isWebViewCorrupted=true"

    .line 640
    invoke-static {v0, p1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    const/4 p1, 0x0

    .line 643
    iput-boolean p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mInAdSplash:Z

    const/4 v1, 0x1

    .line 644
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->requestWindowFeature(I)Z

    const v2, 0x7f0b008a

    .line 645
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->setContentView(I)V

    .line 648
    :try_start_2
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 650
    invoke-virtual {v2}, Landroid/webkit/CookieSyncManager;->startSync()V

    .line 653
    :cond_4
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/UserMgr;->get()Lcom/lltskb/lltskb/engine/online/UserMgr;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/lltskb/lltskb/engine/online/UserMgr;->init(Landroid/content/Context;)V

    .line 654
    invoke-static {}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->get()Lcom/lltskb/lltskb/engine/online/OrderTicketModel;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/lltskb/lltskb/engine/online/OrderTicketModel;->init(Landroid/content/Context;)V

    .line 655
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/lltskb/lltskb/engine/ResMgr;->setContext(Landroid/content/Context;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_2
    move-exception v2

    .line 657
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const v0, 0x7f090018

    .line 660
    invoke-virtual {p0, v0}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_5

    const v2, 0x7f0d02d9

    .line 662
    invoke-virtual {p0, v2}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    .line 663
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object v3

    invoke-virtual {v3}, Lcom/lltskb/lltskb/engine/ResMgr;->getProgVer()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, p1

    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 664
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 666
    :cond_5
    invoke-static {}, Lcom/lltskb/lltskb/engine/ResMgr;->getInstance()Lcom/lltskb/lltskb/engine/ResMgr;

    move-result-object p1

    invoke-virtual {p1}, Lcom/lltskb/lltskb/engine/ResMgr;->isReady()Z

    move-result p1

    if-nez p1, :cond_6

    .line 668
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initResource()V

    goto :goto_3

    .line 670
    :cond_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showSplashAd()V

    .line 675
    :goto_3
    :try_start_3
    invoke-static {}, Lcom/lltskb/lltskb/utils/LocationProvider;->get()Lcom/lltskb/lltskb/utils/LocationProvider;

    move-result-object p1

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/utils/LocationProvider;->init(Landroid/content/Context;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_4

    :catch_3
    move-exception p1

    .line 677
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_4
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    const-string v0, "MainLLTSkb"

    const-string v1, "onCreateOptionsMeanu"

    .line 115
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0x7f0d0193

    .line 116
    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    const v3, 0x7f080145

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 117
    iget v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowType:I

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    const v2, 0x7f0d02c6

    .line 118
    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v2, 0x7f080151

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    :cond_0
    const/4 v0, 0x4

    const v2, 0x7f0d001d

    .line 120
    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v2, 0x7f080059

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    const/4 v0, 0x5

    const v2, 0x7f0d0112

    .line 121
    invoke-interface {p1, v1, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0800c1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 122
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_9

    .line 1165
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-class v2, Lcom/lltskb/lltskb/fragment/SoftFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1166
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1170
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1

    .line 1173
    :cond_0
    const-class p1, Lcom/lltskb/lltskb/fragment/SoftFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->dismissFragment(Ljava/lang/String;)Z

    move-result p1

    const/4 p2, 0x1

    if-eqz p1, :cond_1

    return p2

    .line 1177
    :cond_1
    const-class p1, Lcom/lltskb/lltskb/fragment/AllLifeFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->dismissFragment(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    return p2

    .line 1181
    :cond_2
    const-class p1, Lcom/lltskb/lltskb/fragment/FeedBackFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->dismissFragment(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    return p2

    .line 1185
    :cond_3
    const-class p1, Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/lltskb/lltskb/MainLLTSkb;->dismissFragment(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    return p2

    .line 1189
    :cond_4
    iget-boolean p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mInAdSplash:Z

    if-nez p1, :cond_7

    .line 1190
    iget p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->currTabIdx:I

    if-ne p1, v0, :cond_5

    .line 1191
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mTabPager:Lcom/lltskb/lltskb/view/CustomViewPager;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/lltskb/lltskb/view/CustomViewPager;->setCurrentItem(I)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x3

    if-ne p1, v0, :cond_6

    .line 1192
    iget-boolean p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mLastPressBack:Z

    if-nez p1, :cond_6

    .line 1193
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mDDTabView:Lcom/lltskb/lltskb/action/DDQueryTabView;

    invoke-virtual {p1}, Lcom/lltskb/lltskb/action/DDQueryTabView;->onBack()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 1194
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->tryQuitApp()V

    goto :goto_0

    .line 1197
    :cond_6
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->tryQuitApp()V

    goto :goto_0

    .line 1200
    :cond_7
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V

    :cond_8
    :goto_0
    return p2

    .line 1204
    :cond_9
    invoke-super {p0, p1, p2}, Lcom/lltskb/lltskb/BaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result p1

    return p1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    const-string v0, "MainLLTSkb"

    const-string v1, "onOptionsItemSelected"

    .line 859
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 860
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 878
    :cond_0
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->finish()V

    goto :goto_0

    .line 874
    :cond_1
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->showAbout()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    .line 869
    iput-boolean v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    .line 870
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->update()V

    goto :goto_0

    .line 863
    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v1, "http://wap.lltskb.com/index.html"

    .line 864
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 865
    invoke-static {p0, v0}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 885
    :goto_0
    invoke-super {p0, p1}, Lcom/lltskb/lltskb/BaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method protected onResume()V
    .locals 6

    const-string v0, "MainLLTSkb"

    const-string v1, "onResume"

    .line 1209
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 1210
    invoke-super {p0}, Lcom/lltskb/lltskb/BaseActivity;->onResume()V

    .line 1211
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    const v1, 0x7f090045

    if-nez v0, :cond_0

    .line 1212
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v1, 0x8

    .line 1214
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1217
    :cond_0
    invoke-virtual {p0, v1}, Lcom/lltskb/lltskb/MainLLTSkb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 1219
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1220
    sget-object v2, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    const v3, 0x7f0d006c

    invoke-virtual {p0, v3}, Lcom/lltskb/lltskb/MainLLTSkb;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1221
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public onTask(Landroid/view/View;)V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "onTask"

    .line 828
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-static {}, Lcom/lltskb/lltskb/order/MonitorManager;->get()Lcom/lltskb/lltskb/order/MonitorManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/order/MonitorManager;->size()I

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    .line 830
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 832
    :cond_0
    new-instance p1, Landroid/content/Intent;

    const-class v0, Lcom/lltskb/lltskb/order/MonitorTaskActivity;

    invoke-direct {p1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, p1}, Lcom/lltskb/lltskb/utils/LLTUIUtils;->startActivity(Landroid/app/Activity;Landroid/content/Intent;)V

    return-void
.end method

.method public readConfig()V
    .locals 6

    const-string v0, "MainLLTSkb"

    const-string v1, "readConfig"

    .line 836
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 839
    :try_start_0
    new-instance v2, Ljava/util/Properties;

    invoke-direct {v2}, Ljava/util/Properties;-><init>()V

    .line 840
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "config.properties"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v1

    .line 841
    invoke-virtual {v2, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    const-string v3, "show"

    const-string v4, "3"

    .line 842
    invoke-virtual {v2, v3, v4}, Ljava/util/Properties;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowType:I

    .line 843
    iget v2, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowType:I

    const/4 v3, 0x1

    and-int/2addr v2, v3

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    sput-boolean v3, Lcom/lltskb/lltskb/MainLLTSkb;->mShowAd:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 849
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catchall_0
    move-exception v2

    goto :goto_2

    :catch_0
    move-exception v2

    .line 845
    :try_start_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readConfig open config.properties failed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    .line 849
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    .line 851
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :goto_2
    if-eqz v1, :cond_2

    .line 849
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    :catch_2
    move-exception v1

    .line 851
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 852
    :cond_2
    :goto_3
    throw v2
.end method

.method public selectStation(Landroid/widget/TextView;I)V
    .locals 3

    .line 1912
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "selectStation requestCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MainLLTSkb"

    invoke-static {v1, v0}, Lcom/lltskb/lltskb/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1913
    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f010018

    const v2, 0x7f010019

    .line 1914
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 1915
    new-instance v1, Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-direct {v1}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;-><init>()V

    .line 1917
    invoke-virtual {v1, p1, p2}, Lcom/lltskb/lltskb/fragment/SelectStationFragment;->setTextView(Landroid/widget/TextView;I)V

    .line 1918
    const-class p1, Lcom/lltskb/lltskb/fragment/SelectStationFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    const p2, 0x7f0900e1

    invoke-virtual {v0, p2, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 1920
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    return-void
.end method

.method public showSelectDlg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 2

    const-string v0, "MainLLTSkb"

    const-string v1, "showSelectDlg"

    .line 965
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->prepareProg()V

    .line 968
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 970
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 972
    :goto_0
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 973
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v0, p1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 974
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 975
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$0zXUnyONEm8KRAgLhmIczkV4PZE;

    invoke-direct {p2, p0, p3}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$0zXUnyONEm8KRAgLhmIczkV4PZE;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;Ljava/lang/Runnable;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setYesBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 992
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    new-instance p2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$LSHu5z6-bI0Ggp2phFjtFcBvrRk;

    invoke-direct {p2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$LSHu5z6-bI0Ggp2phFjtFcBvrRk;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {p1, p2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setNoBtnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public update()V
    .locals 4

    const-string v0, "MainLLTSkb"

    const-string v1, "update"

    .line 914
    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    invoke-direct {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->prepareProg()V

    .line 917
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 919
    :cond_0
    iget-boolean v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mShowUpdateUI:Z

    if-eqz v1, :cond_2

    .line 920
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setStyle(I)V

    .line 922
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0099

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 923
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {p0}, Lcom/lltskb/lltskb/MainLLTSkb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d00b9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setMessage(Ljava/lang/String;)V

    .line 924
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setIndeterminate(Z)V

    .line 925
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    new-instance v2, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$7-a4iCYNHOlo1-J-GCvQigwxzxQ;

    invoke-direct {v2, p0}, Lcom/lltskb/lltskb/-$$Lambda$MainLLTSkb$7-a4iCYNHOlo1-J-GCvQigwxzxQ;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-virtual {v1, v2}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->setCancelBtnClickListener(Landroid/view/View$OnClickListener;)V

    .line 926
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    const v2, 0x7f0e00bd

    .line 928
    invoke-virtual {v1, v2}, Landroid/view/Window;->setWindowAnimations(I)V

    .line 932
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/lltskb/lltskb/MainLLTSkb;->mProgDialog:Lcom/lltskb/lltskb/view/LLTProgressDialog;

    invoke-virtual {v1}, Lcom/lltskb/lltskb/view/LLTProgressDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 934
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/lltskb/lltskb/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 937
    :cond_2
    :goto_0
    invoke-static {}, Lcom/lltskb/lltskb/AppContext;->get()Lcom/lltskb/lltskb/AppContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppContext;->getExecutors()Lcom/lltskb/lltskb/AppExecutors;

    move-result-object v0

    invoke-virtual {v0}, Lcom/lltskb/lltskb/AppExecutors;->networkIO()Ljava/util/concurrent/Executor;

    move-result-object v0

    new-instance v1, Lcom/lltskb/lltskb/-$$Lambda$Cx7xJqRT36g6VHJwton731ouWrk;

    invoke-direct {v1, p0}, Lcom/lltskb/lltskb/-$$Lambda$Cx7xJqRT36g6VHJwton731ouWrk;-><init>(Lcom/lltskb/lltskb/MainLLTSkb;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_3
    :goto_1
    return-void
.end method
