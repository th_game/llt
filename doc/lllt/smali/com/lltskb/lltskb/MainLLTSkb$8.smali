.class Lcom/lltskb/lltskb/MainLLTSkb$8;
.super Landroid/support/v4/view/PagerAdapter;
.source "MainLLTSkb.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lltskb/lltskb/MainLLTSkb;->initMainUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lltskb/lltskb/MainLLTSkb;

.field final synthetic val$views:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/lltskb/lltskb/MainLLTSkb;Ljava/util/ArrayList;)V
    .locals 0

    .line 758
    iput-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->this$0:Lcom/lltskb/lltskb/MainLLTSkb;

    iput-object p2, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->val$views:Ljava/util/ArrayList;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 772
    iget-object p3, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->val$views:Ljava/util/ArrayList;

    invoke-virtual {p3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 767
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->val$views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .line 783
    iget-object v0, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->val$views:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 784
    iget-object p1, p0, Lcom/lltskb/lltskb/MainLLTSkb$8;->val$views:Ljava/util/ArrayList;

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
