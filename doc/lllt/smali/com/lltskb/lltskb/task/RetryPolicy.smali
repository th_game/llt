.class public Lcom/lltskb/lltskb/task/RetryPolicy;
.super Ljava/lang/Object;
.source "RetryPolicy.java"


# instance fields
.field private delayTime:I

.field private maxAttemptLeft:I

.field private timeout:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    .line 9
    iput v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->maxAttemptLeft:I

    const/4 v0, 0x0

    .line 10
    iput v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->delayTime:I

    const/16 v0, 0x1e

    .line 11
    iput v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->timeout:I

    return-void
.end method


# virtual methods
.method public getDelayTime()I
    .locals 1

    .line 18
    iget v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->delayTime:I

    return v0
.end method

.method public getMaxAttemptLeft()I
    .locals 1

    .line 14
    iget v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->maxAttemptLeft:I

    return v0
.end method

.method public getTimeout()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->timeout:I

    return v0
.end method

.method public setDelayTime(I)Lcom/lltskb/lltskb/task/RetryPolicy;
    .locals 0

    .line 31
    iput p1, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->delayTime:I

    return-object p0
.end method

.method public setMaxAttemptLeft(I)Lcom/lltskb/lltskb/task/RetryPolicy;
    .locals 0

    .line 26
    iput p1, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->maxAttemptLeft:I

    return-object p0
.end method

.method public setTimeout(I)Lcom/lltskb/lltskb/task/RetryPolicy;
    .locals 0

    .line 36
    iput p1, p0, Lcom/lltskb/lltskb/task/RetryPolicy;->timeout:I

    return-object p0
.end method
