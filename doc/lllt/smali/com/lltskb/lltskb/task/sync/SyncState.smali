.class public final enum Lcom/lltskb/lltskb/task/sync/SyncState;
.super Ljava/lang/Enum;
.source "SyncState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/lltskb/lltskb/task/sync/SyncState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/lltskb/lltskb/task/sync/SyncState;

.field public static final enum EXECUTING:Lcom/lltskb/lltskb/task/sync/SyncState;

.field public static final enum FAULTED:Lcom/lltskb/lltskb/task/sync/SyncState;

.field public static final enum PREPARING:Lcom/lltskb/lltskb/task/sync/SyncState;

.field public static final enum READY:Lcom/lltskb/lltskb/task/sync/SyncState;

.field public static final enum SUCCESS:Lcom/lltskb/lltskb/task/sync/SyncState;


# instance fields
.field private state:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 9
    new-instance v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PREPARING"

    invoke-direct {v0, v3, v1, v2}, Lcom/lltskb/lltskb/task/sync/SyncState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->PREPARING:Lcom/lltskb/lltskb/task/sync/SyncState;

    new-instance v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    const/4 v3, 0x2

    const-string v4, "READY"

    invoke-direct {v0, v4, v2, v3}, Lcom/lltskb/lltskb/task/sync/SyncState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->READY:Lcom/lltskb/lltskb/task/sync/SyncState;

    new-instance v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    const/4 v4, 0x3

    const-string v5, "EXECUTING"

    invoke-direct {v0, v5, v3, v4}, Lcom/lltskb/lltskb/task/sync/SyncState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->EXECUTING:Lcom/lltskb/lltskb/task/sync/SyncState;

    new-instance v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    const/4 v5, 0x4

    const-string v6, "FAULTED"

    invoke-direct {v0, v6, v4, v5}, Lcom/lltskb/lltskb/task/sync/SyncState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->FAULTED:Lcom/lltskb/lltskb/task/sync/SyncState;

    new-instance v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    const/4 v6, 0x5

    const-string v7, "SUCCESS"

    invoke-direct {v0, v7, v5, v6}, Lcom/lltskb/lltskb/task/sync/SyncState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->SUCCESS:Lcom/lltskb/lltskb/task/sync/SyncState;

    new-array v0, v6, [Lcom/lltskb/lltskb/task/sync/SyncState;

    .line 8
    sget-object v6, Lcom/lltskb/lltskb/task/sync/SyncState;->PREPARING:Lcom/lltskb/lltskb/task/sync/SyncState;

    aput-object v6, v0, v1

    sget-object v1, Lcom/lltskb/lltskb/task/sync/SyncState;->READY:Lcom/lltskb/lltskb/task/sync/SyncState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/lltskb/lltskb/task/sync/SyncState;->EXECUTING:Lcom/lltskb/lltskb/task/sync/SyncState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/lltskb/lltskb/task/sync/SyncState;->FAULTED:Lcom/lltskb/lltskb/task/sync/SyncState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/lltskb/lltskb/task/sync/SyncState;->SUCCESS:Lcom/lltskb/lltskb/task/sync/SyncState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->$VALUES:[Lcom/lltskb/lltskb/task/sync/SyncState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12
    iput p3, p0, Lcom/lltskb/lltskb/task/sync/SyncState;->state:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/lltskb/lltskb/task/sync/SyncState;
    .locals 1

    .line 8
    const-class v0, Lcom/lltskb/lltskb/task/sync/SyncState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/lltskb/lltskb/task/sync/SyncState;

    return-object p0
.end method

.method public static values()[Lcom/lltskb/lltskb/task/sync/SyncState;
    .locals 1

    .line 8
    sget-object v0, Lcom/lltskb/lltskb/task/sync/SyncState;->$VALUES:[Lcom/lltskb/lltskb/task/sync/SyncState;

    invoke-virtual {v0}, [Lcom/lltskb/lltskb/task/sync/SyncState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/lltskb/lltskb/task/sync/SyncState;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .line 17
    iget v0, p0, Lcom/lltskb/lltskb/task/sync/SyncState;->state:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
