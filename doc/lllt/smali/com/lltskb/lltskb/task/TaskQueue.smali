.class public Lcom/lltskb/lltskb/task/TaskQueue;
.super Ljava/lang/Object;
.source "TaskQueue.java"


# static fields
.field private static instance:Lcom/lltskb/lltskb/task/TaskQueue;


# instance fields
.field executorService:Ljava/util/concurrent/ExecutorService;

.field taskQueue:Ljava/util/concurrent/LinkedBlockingDeque;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Lcom/lltskb/lltskb/task/TaskQueue;->taskQueue:Ljava/util/concurrent/LinkedBlockingDeque;

    .line 24
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/lltskb/lltskb/task/TaskQueue;->executorService:Ljava/util/concurrent/ExecutorService;

    return-void
.end method


# virtual methods
.method public declared-synchronized get()Lcom/lltskb/lltskb/task/TaskQueue;
    .locals 1

    monitor-enter p0

    .line 17
    :try_start_0
    sget-object v0, Lcom/lltskb/lltskb/task/TaskQueue;->instance:Lcom/lltskb/lltskb/task/TaskQueue;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/lltskb/lltskb/task/TaskQueue;

    invoke-direct {v0}, Lcom/lltskb/lltskb/task/TaskQueue;-><init>()V

    sput-object v0, Lcom/lltskb/lltskb/task/TaskQueue;->instance:Lcom/lltskb/lltskb/task/TaskQueue;

    .line 20
    :cond_0
    sget-object v0, Lcom/lltskb/lltskb/task/TaskQueue;->instance:Lcom/lltskb/lltskb/task/TaskQueue;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
