.class public Lcom/qq/e/comm/constants/LoadAdParams;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:Lcom/qq/e/comm/constants/LoginType;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBlockEffectValue()I
    .locals 1

    iget v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->f:I

    return v0
.end method

.method public getFlowSourceId()I
    .locals 1

    iget v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->a:I

    return v0
.end method

.method public getLoginAppId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->c:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginOpenid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getLoginType()Lcom/qq/e/comm/constants/LoginType;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->b:Lcom/qq/e/comm/constants/LoginType;

    return-object v0
.end method

.method public getUin()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/qq/e/comm/constants/LoadAdParams;->e:Ljava/lang/String;

    return-object v0
.end method

.method public setBlockEffectValue(I)V
    .locals 0

    iput p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->f:I

    return-void
.end method

.method public setFlowSourceId(I)V
    .locals 0

    iput p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->a:I

    return-void
.end method

.method public setLoginAppId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->c:Ljava/lang/String;

    return-void
.end method

.method public setLoginOpenid(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->d:Ljava/lang/String;

    return-void
.end method

.method public setLoginType(Lcom/qq/e/comm/constants/LoginType;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->b:Lcom/qq/e/comm/constants/LoginType;

    return-void
.end method

.method public setUin(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/comm/constants/LoadAdParams;->e:Ljava/lang/String;

    return-void
.end method
