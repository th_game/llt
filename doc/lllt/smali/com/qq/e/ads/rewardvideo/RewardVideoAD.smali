.class public Lcom/qq/e/ads/rewardvideo/RewardVideoAD;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qq/e/ads/rewardvideo/RewardVideoAD$ADListenerAdapter;
    }
.end annotation


# instance fields
.field private a:Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;

.field private volatile b:Z

.field private c:Lcom/qq/e/comm/pi/RVADI;

.field private volatile d:Z

.field private volatile e:Z

.field private volatile f:Z

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;)V
    .locals 8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->h:Ljava/util/Map;

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    iput-object p4, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;

    iput-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->d:Z

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string p1, "Required Activity/Service/Permission Not Declared in AndroidManifest.xml"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/16 p1, 0xfa2

    invoke-direct {p0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->a(I)V

    return-void

    :cond_1
    iput-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->e:Z

    iput-object p3, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->g:Ljava/lang/String;

    sget-object v0, Lcom/qq/e/comm/managers/GDTADManager;->INIT_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/qq/e/ads/rewardvideo/RewardVideoAD$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD$1;-><init>(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;Landroid/content/Context;Ljava/lang/String;Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_2
    :goto_0
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object p2, v1, v0

    const/4 p1, 0x2

    aput-object p3, v1, p1

    const/4 p1, 0x3

    aput-object p4, v1, p1

    const-string p1, "RewardVideoAD Constructor params error, context=%s, appID=%s, posID=%s, rewardVideoADListener=%s"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;Lcom/qq/e/comm/pi/RVADI;)Lcom/qq/e/comm/pi/RVADI;
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/qq/e/ads/rewardvideo/RewardVideoAD$2;

    invoke-direct {v1, p0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD$2;-><init>(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;I)V
    .locals 0

    const/16 p1, 0x7d1

    invoke-direct {p0, p1}, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->f:Z

    return p0
.end method

.method static synthetic a(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;Z)Z
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->b:Z

    return p1
.end method

.method static synthetic b(Lcom/qq/e/ads/rewardvideo/RewardVideoAD;)Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->a:Lcom/qq/e/ads/rewardvideo/RewardVideoADListener;

    return-object p0
.end method


# virtual methods
.method public getECPM()I
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/RVADI;->getECPM()I

    move-result v0

    return v0

    :cond_0
    const-string v0, "please invoke getECPM method after callback \"onADLoad\" "

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/4 v0, -0x1

    return v0
.end method

.method public getExpireTimestamp()J
    .locals 2

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/RVADI;->getExpireTimestamp()J

    move-result-wide v0

    return-wide v0

    :cond_0
    const-string v0, "please invoke getExpireTimestamp method after callback \"onADLoad\" "

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getExts()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->h:Ljava/util/Map;

    return-object v0
.end method

.method public hasShown()Z
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/RVADI;->hasShown()Z

    move-result v0

    return v0

    :cond_0
    const-string v0, "please invoke hasShown method after callback \"onADLoad\" "

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/4 v0, 0x0

    return v0
.end method

.method public loadAD()V
    .locals 1

    iget-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->d:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->e:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->b:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->f:Z

    return-void

    :cond_1
    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/qq/e/comm/pi/RVADI;->loadAD()V

    return-void

    :cond_2
    const-string v0, "RewardVideo AD Init error, see more logs"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_0
    const-string v0, "AD init Params OR Context error, details in logs produced while init RewardVideoAD"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method public setExt(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->h:Ljava/util/Map;

    const-string v1, "clickUrl"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setTag(Ljava/util/Map;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/qq/e/comm/managers/GDTADManager;->getSM()Lcom/qq/e/comm/managers/setting/SM;

    move-result-object p1

    const-string v1, "ad_tags"

    iget-object v2, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->g:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, Lcom/qq/e/comm/managers/setting/SM;->setDEVCodeSetting(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string v0, "NativeUnifiedAD#setTag Exception"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-void
.end method

.method public showAD()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/rewardvideo/RewardVideoAD;->c:Lcom/qq/e/comm/pi/RVADI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/RVADI;->showAD()V

    return-void

    :cond_0
    const-string v0, "please invoke showAD method after callback \"onADLoad\" "

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method
