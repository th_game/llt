.class public Lcom/qq/e/ads/banner2/UnifiedBannerView;
.super Landroid/widget/FrameLayout;


# instance fields
.field private a:Lcom/qq/e/comm/pi/UBVI;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

.field private volatile f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/banner2/UnifiedBannerADListener;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/qq/e/ads/banner2/UnifiedBannerView;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/banner2/UnifiedBannerADListener;Ljava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/banner2/UnifiedBannerADListener;Ljava/util/Map;)V
    .locals 9

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->b:Z

    iput-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->c:Z

    iput-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->d:Z

    iput v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->f:I

    const/16 v1, 0x1e

    iput v1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->g:I

    invoke-static {p2}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-static {p3}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    if-eqz p1, :cond_2

    if-nez p4, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v2, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->b:Z

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string p1, "Required Activity/Service/Permission Not Declared in AndroidManifest.xml"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->c:Z

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/qq/e/ads/banner2/UnifiedBannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v0, Lcom/qq/e/comm/managers/GDTADManager;->INIT_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    new-instance v8, Lcom/qq/e/ads/banner2/UnifiedBannerView$1;

    move-object v1, v8

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/qq/e/ads/banner2/UnifiedBannerView$1;-><init>(Lcom/qq/e/ads/banner2/UnifiedBannerView;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/banner2/UnifiedBannerADListener;Ljava/util/Map;)V

    invoke-interface {v0, v8}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_2
    :goto_0
    const/4 p5, 0x4

    new-array p5, p5, [Ljava/lang/Object;

    aput-object p2, p5, v0

    aput-object p3, p5, v2

    const/4 p2, 0x2

    aput-object p1, p5, p2

    const/4 p1, 0x3

    aput-object p4, p5, p1

    const-string p1, "UnifiedBannerView Constructor params error, appid=%s,posId=%s,context=%s,listener=%s"

    invoke-static {p1, p5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/banner2/UnifiedBannerView;)Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->e:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    return-object p0
.end method

.method static synthetic a(Lcom/qq/e/ads/banner2/UnifiedBannerView;Lcom/qq/e/comm/pi/UBVI;)Lcom/qq/e/comm/pi/UBVI;
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    return-object p1
.end method

.method static synthetic a(Lcom/qq/e/ads/banner2/UnifiedBannerView;Ljava/util/Map;Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result p0

    if-lez p0, :cond_0

    :try_start_0
    new-instance p0, Lorg/json/JSONObject;

    invoke-direct {p0, p1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object p1

    invoke-virtual {p1}, Lcom/qq/e/comm/managers/GDTADManager;->getSM()Lcom/qq/e/comm/managers/setting/SM;

    move-result-object p1

    const-string v0, "ad_tags"

    invoke-virtual {p1, v0, p0, p2}, Lcom/qq/e/comm/managers/setting/SM;->setDEVCodeSetting(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p0

    const-string p1, "UnifiedBanner#setTag Exception"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/banner2/UnifiedBannerView;Z)Z
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/qq/e/ads/banner2/UnifiedBannerView;)I
    .locals 0

    iget p0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->g:I

    return p0
.end method

.method static synthetic c(Lcom/qq/e/ads/banner2/UnifiedBannerView;)I
    .locals 2

    iget v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->f:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->f:I

    return v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UBVI;->destroy()V

    :cond_0
    return-void
.end method

.method public getExt()Ljava/util/Map;
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/qq/e/comm/pi/UBVI;->ext:Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    const-string v0, "banner2 can not get ext"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public loadAD()V
    .locals 1

    iget-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->b:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->c:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->d:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->f:I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/qq/e/comm/pi/UBVI;->fetchAd()V

    return-void

    :cond_2
    const-string v0, "UnifiedBannerView Init error,See More Logs"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_0
    const-string v0, "UnifiedBannerView init Paras OR Context error,See More logs while new BannerView"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UBVI;->onWindowFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public setDownConfirmPolicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V
    .locals 1

    iput-object p1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->e:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UBVI;->setDownAPPConfirmPolicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V

    :cond_0
    return-void
.end method

.method public setRefresh(I)V
    .locals 1

    iput p1, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->g:I

    iget-object v0, p0, Lcom/qq/e/ads/banner2/UnifiedBannerView;->a:Lcom/qq/e/comm/pi/UBVI;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/UBVI;->setRefresh(I)V

    :cond_0
    return-void
.end method
