.class public Lcom/qq/e/ads/nativ/MediaListenerAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/adevent/ADListener;


# instance fields
.field private a:Lcom/qq/e/ads/nativ/MediaListener;


# direct methods
.method public constructor <init>(Lcom/qq/e/ads/nativ/MediaListener;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    return-void
.end method


# virtual methods
.method public onADEvent(Lcom/qq/e/comm/adevent/ADEvent;)V
    .locals 4

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getType()I

    move-result v0

    const-string v1, "NativeMedia ADEvent Paras error!"

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/nativ/MediaListener;->onFullScreenChanged(Z)V

    return-void

    :cond_0
    invoke-static {v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/MediaListener;->onADButtonClicked()V

    return-void

    :pswitch_2
    iget-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/MediaListener;->onReplayButtonClicked()V

    return-void

    :pswitch_3
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(I)Lcom/qq/e/comm/util/AdError;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/nativ/MediaListener;->onVideoError(Lcom/qq/e/comm/util/AdError;)V

    return-void

    :cond_1
    invoke-static {v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :pswitch_4
    iget-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/MediaListener;->onVideoComplete()V

    return-void

    :pswitch_5
    iget-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/MediaListener;->onVideoPause()V

    return-void

    :pswitch_6
    iget-object p1, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-interface {p1}, Lcom/qq/e/ads/nativ/MediaListener;->onVideoStart()V

    return-void

    :pswitch_7
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/qq/e/ads/nativ/MediaListenerAdapter;->a:Lcom/qq/e/ads/nativ/MediaListener;

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v1, p1

    invoke-interface {v0, v1, v2}, Lcom/qq/e/ads/nativ/MediaListener;->onVideoReady(J)V

    return-void

    :cond_2
    invoke-static {v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
