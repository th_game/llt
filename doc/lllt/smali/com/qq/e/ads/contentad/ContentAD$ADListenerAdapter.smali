.class Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/adevent/ADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qq/e/ads/contentad/ContentAD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ADListenerAdapter"
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/ads/contentad/ContentAD;


# direct methods
.method private constructor <init>(Lcom/qq/e/ads/contentad/ContentAD;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qq/e/ads/contentad/ContentAD;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;-><init>(Lcom/qq/e/ads/contentad/ContentAD;)V

    return-void
.end method


# virtual methods
.method public onADEvent(Lcom/qq/e/comm/adevent/ADEvent;)V
    .locals 7

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, "No DevADListener Binded"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->i(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getType()I

    move-result v0

    const-string v1, ")"

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v2, :cond_9

    const/4 v4, 0x2

    const-string v5, "ADEvent.Paras error for ContentAD("

    if-eq v0, v4, :cond_7

    const/4 v6, 0x3

    if-eq v0, v6, :cond_5

    const/4 v6, 0x4

    if-eq v0, v6, :cond_3

    const/4 v4, 0x5

    if-eq v0, v4, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_2

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Lcom/qq/e/ads/nativ/NativeMediaADData;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Lcom/qq/e/ads/nativ/NativeMediaADData;

    invoke-interface {v0, p1}, Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;->onADVideoLoaded(Lcom/qq/e/ads/contentad/ContentAdData;)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v4, :cond_4

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Lcom/qq/e/ads/contentad/ContentAdData;

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v2

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v1

    aget-object v1, v1, v3

    check-cast v1, Lcom/qq/e/ads/contentad/ContentAdData;

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v2

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, v1, p1}, Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;->onContentADError(Lcom/qq/e/ads/contentad/ContentAdData;I)V

    return-void

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_5
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_6

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Lcom/qq/e/ads/nativ/NativeMediaADData;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Lcom/qq/e/ads/nativ/NativeMediaADData;

    invoke-interface {v0, p1}, Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;->onContentADStatusChanged(Lcom/qq/e/ads/contentad/ContentAdData;)V

    return-void

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_7
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_8

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Ljava/util/List;

    invoke-interface {v0, p1}, Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;->onContentADLoaded(Ljava/util/List;)V

    return-void

    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_9
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v2, :cond_a

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    aget-object v0, v0, v3

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/qq/e/ads/contentad/ContentAD$ADListenerAdapter;->a:Lcom/qq/e/ads/contentad/ContentAD;

    invoke-static {v0}, Lcom/qq/e/ads/contentad/ContentAD;->a(Lcom/qq/e/ads/contentad/ContentAD;)Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v3

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/contentad/ContentAD$ContentADListener;->onNoContentAD(I)V

    return-void

    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "AdEvent.Paras error for ContentAD("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method
