.class Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qq/e/ads/interstitial/InterstitialAD$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/comm/pi/POFactory;

.field private synthetic b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;


# direct methods
.method constructor <init>(Lcom/qq/e/ads/interstitial/InterstitialAD$1;Lcom/qq/e/comm/pi/POFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iput-object p2, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    iget-object v2, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    iget-object v3, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v3, v3, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->a:Landroid/app/Activity;

    iget-object v4, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v4, v4, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v5, v5, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->c:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/qq/e/comm/pi/POFactory;->getIADView(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)Lcom/qq/e/comm/pi/IADI;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/qq/e/ads/interstitial/InterstitialAD;->a(Lcom/qq/e/ads/interstitial/InterstitialAD;Lcom/qq/e/comm/pi/IADI;)Lcom/qq/e/comm/pi/IADI;

    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-static {v1}, Lcom/qq/e/ads/interstitial/InterstitialAD;->a(Lcom/qq/e/ads/interstitial/InterstitialAD;)Lcom/qq/e/comm/pi/IADI;

    move-result-object v1

    new-instance v2, Lcom/qq/e/ads/interstitial/InterstitialAD$ADListenerAdapter;

    iget-object v3, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v3, v3, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/qq/e/ads/interstitial/InterstitialAD$ADListenerAdapter;-><init>(Lcom/qq/e/ads/interstitial/InterstitialAD;B)V

    invoke-interface {v1, v2}, Lcom/qq/e/comm/pi/IADI;->setAdListener(Lcom/qq/e/comm/adevent/ADListener;)V

    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-static {v1, v0}, Lcom/qq/e/ads/interstitial/InterstitialAD;->a(Lcom/qq/e/ads/interstitial/InterstitialAD;Z)Z

    :goto_0
    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-static {v1}, Lcom/qq/e/ads/interstitial/InterstitialAD;->b(Lcom/qq/e/ads/interstitial/InterstitialAD;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-virtual {v1}, Lcom/qq/e/ads/interstitial/InterstitialAD;->loadAD()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v1, v1, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-static {v1, v0}, Lcom/qq/e/ads/interstitial/InterstitialAD;->a(Lcom/qq/e/ads/interstitial/InterstitialAD;Z)Z

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    const-string v2, "Exception while init IAD Core"

    invoke-static {v2, v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/qq/e/ads/interstitial/InterstitialAD$1$1;->b:Lcom/qq/e/ads/interstitial/InterstitialAD$1;

    iget-object v2, v2, Lcom/qq/e/ads/interstitial/InterstitialAD$1;->d:Lcom/qq/e/ads/interstitial/InterstitialAD;

    invoke-static {v2, v0}, Lcom/qq/e/ads/interstitial/InterstitialAD;->a(Lcom/qq/e/ads/interstitial/InterstitialAD;Z)Z

    goto :goto_3

    :goto_2
    throw v1

    :goto_3
    goto :goto_2
.end method
