.class public Lcom/qq/e/ads/banner/BannerView;
.super Landroid/widget/FrameLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;
    }
.end annotation


# instance fields
.field private a:Lcom/qq/e/comm/pi/BVI;

.field private b:Lcom/qq/e/ads/banner/BannerADListener;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/Integer;

.field private g:Lcom/qq/e/ads/cfg/BannerRollAnimation;

.field private h:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

.field private i:Ljava/lang/Boolean;

.field private volatile j:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/qq/e/ads/banner/ADSize;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->c:Z

    iput-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->d:Z

    iput-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->e:Z

    iput v0, p0, Lcom/qq/e/ads/banner/BannerView;->j:I

    invoke-static {p3}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_2

    invoke-static {p4}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    iput-boolean v2, p0, Lcom/qq/e/ads/banner/BannerView;->c:Z

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string p1, "Required Activity/Service/Permission Not Declared in AndroidManifest.xml"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/qq/e/ads/banner/BannerView;->d:Z

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/qq/e/ads/banner/BannerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v0, Lcom/qq/e/comm/managers/GDTADManager;->INIT_EXECUTOR:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/qq/e/ads/banner/BannerView$1;

    move-object v1, v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/qq/e/ads/banner/BannerView$1;-><init>(Lcom/qq/e/ads/banner/BannerView;Landroid/app/Activity;Ljava/lang/String;Lcom/qq/e/ads/banner/ADSize;Ljava/lang/String;)V

    invoke-interface {v0, v7}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void

    :cond_2
    :goto_0
    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p3, p2, v0

    aput-object p4, p2, v2

    const/4 p3, 0x2

    aput-object p1, p2, p3

    const-string p1, "Banner ADView Constructor params error, appid=%s,posId=%s,context=%s"

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/comm/pi/BVI;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    return-object p0
.end method

.method static synthetic a(Lcom/qq/e/ads/banner/BannerView;Lcom/qq/e/comm/pi/BVI;)Lcom/qq/e/comm/pi/BVI;
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    return-object p1
.end method

.method static synthetic a(Lcom/qq/e/ads/banner/BannerView;Z)Z
    .locals 0

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/qq/e/ads/banner/BannerView;->e:Z

    return p1
.end method

.method static synthetic b(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->h:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    return-object p0
.end method

.method static synthetic c(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Integer;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->f:Ljava/lang/Integer;

    return-object p0
.end method

.method static synthetic d(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/BannerRollAnimation;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->g:Lcom/qq/e/ads/cfg/BannerRollAnimation;

    return-object p0
.end method

.method static synthetic e(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Boolean;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->i:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic f(Lcom/qq/e/ads/banner/BannerView;)I
    .locals 2

    iget v0, p0, Lcom/qq/e/ads/banner/BannerView;->j:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/qq/e/ads/banner/BannerView;->j:I

    return v0
.end method

.method static synthetic g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/banner/BannerView;->b:Lcom/qq/e/ads/banner/BannerADListener;

    return-object p0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/qq/e/comm/pi/BVI;->destroy()V

    :cond_0
    return-void
.end method

.method public loadAD()V
    .locals 1

    iget-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->c:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->d:Z

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/qq/e/ads/banner/BannerView;->e:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/qq/e/ads/banner/BannerView;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/qq/e/ads/banner/BannerView;->j:I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/qq/e/comm/pi/BVI;->fetchAd()V

    return-void

    :cond_2
    const-string v0, "Banner Init error,See More Logs"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void

    :cond_3
    :goto_0
    const-string v0, "Banner init Paras OR Context error,See More logs while new BannerView"

    invoke-static {v0}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    return-void
.end method

.method public setADListener(Lcom/qq/e/ads/banner/BannerADListener;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView;->b:Lcom/qq/e/ads/banner/BannerADListener;

    return-void
.end method

.method public setDownConfirmPilicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V
    .locals 1

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView;->h:Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;->value()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/BVI;->setDownAPPConfirmPolicy(I)V

    :cond_0
    return-void
.end method

.method public setRefresh(I)V
    .locals 2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->f:Ljava/lang/Integer;

    const/16 v0, 0x78

    const/16 v1, 0x1e

    if-ge p1, v1, :cond_0

    if-eqz p1, :cond_0

    const/16 p1, 0x1e

    goto :goto_0

    :cond_0
    if-le p1, v0, :cond_1

    const/16 p1, 0x78

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_2

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/BVI;->setRefresh(I)V

    :cond_2
    return-void
.end method

.method public setRollAnimation(Lcom/qq/e/ads/cfg/BannerRollAnimation;)V
    .locals 1

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView;->g:Lcom/qq/e/ads/cfg/BannerRollAnimation;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/qq/e/ads/cfg/BannerRollAnimation;->value()I

    move-result p1

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/BVI;->setRollAnimation(I)V

    :cond_0
    return-void
.end method

.method public setShowClose(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->i:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView;->a:Lcom/qq/e/comm/pi/BVI;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/BVI;->setShowCloseButton(Z)V

    :cond_0
    return-void
.end method
