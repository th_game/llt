.class Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/qq/e/comm/adevent/ADListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qq/e/ads/banner/BannerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ADListenerAdapter"
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/ads/banner/BannerView;


# direct methods
.method private constructor <init>(Lcom/qq/e/ads/banner/BannerView;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qq/e/ads/banner/BannerView;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;-><init>(Lcom/qq/e/ads/banner/BannerView;)V

    return-void
.end method


# virtual methods
.method public onADEvent(Lcom/qq/e/comm/adevent/ADEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v0}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object v0

    if-nez v0, :cond_0

    const-string p1, "No DevADListener Binded"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->i(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADCloseOverlay()V

    goto/16 :goto_0

    :pswitch_1
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADOpenOverlay()V

    return-void

    :pswitch_2
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADLeftApplication()V

    return-void

    :pswitch_3
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADClicked()V

    return-void

    :pswitch_4
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADClosed()V

    return-void

    :pswitch_5
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADExposure()V

    return-void

    :pswitch_6
    iget-object p1, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {p1}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/qq/e/ads/banner/BannerADListener;->onADReceiv()V

    return-void

    :pswitch_7
    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;->a:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v0}, Lcom/qq/e/ads/banner/BannerView;->g(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/banner/BannerADListener;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qq/e/comm/adevent/ADEvent;->getParas()[Ljava/lang/Object;

    move-result-object p1

    aget-object p1, p1, v1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(I)Lcom/qq/e/comm/util/AdError;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/qq/e/ads/banner/BannerADListener;->onNoAD(Lcom/qq/e/comm/util/AdError;)V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AdEvent.Paras error for Banner("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
