.class Lcom/qq/e/ads/banner/BannerView$1$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qq/e/ads/banner/BannerView$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private synthetic a:Lcom/qq/e/comm/pi/POFactory;

.field private synthetic b:Lcom/qq/e/ads/banner/BannerView$1;


# direct methods
.method constructor <init>(Lcom/qq/e/ads/banner/BannerView$1;Lcom/qq/e/comm/pi/POFactory;)V
    .locals 0

    iput-object p1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iput-object p2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->a:Lcom/qq/e/comm/pi/POFactory;

    iget-object v3, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v3, v3, Lcom/qq/e/ads/banner/BannerView$1;->a:Landroid/app/Activity;

    iget-object v4, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v4, v4, Lcom/qq/e/ads/banner/BannerView$1;->c:Lcom/qq/e/ads/banner/ADSize;

    iget-object v5, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v5, v5, Lcom/qq/e/ads/banner/BannerView$1;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v6, v6, Lcom/qq/e/ads/banner/BannerView$1;->d:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/qq/e/comm/pi/POFactory;->getBannerView(Landroid/app/Activity;Lcom/qq/e/ads/banner/ADSize;Ljava/lang/String;Ljava/lang/String;)Lcom/qq/e/comm/pi/BVI;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;Lcom/qq/e/comm/pi/BVI;)Lcom/qq/e/comm/pi/BVI;

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/comm/pi/BVI;

    move-result-object v1

    new-instance v2, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;

    iget-object v3, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v3, v3, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/qq/e/ads/banner/BannerView$ADListenerAdapter;-><init>(Lcom/qq/e/ads/banner/BannerView;B)V

    invoke-interface {v1, v2}, Lcom/qq/e/comm/pi/BVI;->setAdListener(Lcom/qq/e/comm/adevent/ADListener;)V

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/comm/pi/BVI;

    move-result-object v2

    invoke-interface {v2}, Lcom/qq/e/comm/pi/BVI;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1, v0}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;Z)Z

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->b(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2}, Lcom/qq/e/ads/banner/BannerView;->b(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->setDownConfirmPilicy(Lcom/qq/e/ads/cfg/DownAPPConfirmPolicy;)V

    :cond_0
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->c(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2}, Lcom/qq/e/ads/banner/BannerView;->c(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->setRefresh(I)V

    :cond_1
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->d(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/BannerRollAnimation;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2}, Lcom/qq/e/ads/banner/BannerView;->d(Lcom/qq/e/ads/banner/BannerView;)Lcom/qq/e/ads/cfg/BannerRollAnimation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->setRollAnimation(Lcom/qq/e/ads/cfg/BannerRollAnimation;)V

    :cond_2
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->e(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2}, Lcom/qq/e/ads/banner/BannerView;->e(Lcom/qq/e/ads/banner/BannerView;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/qq/e/ads/banner/BannerView;->setShowClose(Z)V

    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1}, Lcom/qq/e/ads/banner/BannerView;->f(Lcom/qq/e/ads/banner/BannerView;)I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-virtual {v1}, Lcom/qq/e/ads/banner/BannerView;->loadAD()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_4
    :goto_1
    iget-object v1, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v1, v1, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v1, v0}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;Z)Z

    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    const-string v2, "Exception while init Banner Core"

    invoke-static {v2, v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    iget-object v2, p0, Lcom/qq/e/ads/banner/BannerView$1$1;->b:Lcom/qq/e/ads/banner/BannerView$1;

    iget-object v2, v2, Lcom/qq/e/ads/banner/BannerView$1;->e:Lcom/qq/e/ads/banner/BannerView;

    invoke-static {v2, v0}, Lcom/qq/e/ads/banner/BannerView;->a(Lcom/qq/e/ads/banner/BannerView;Z)Z

    goto :goto_3

    :goto_2
    throw v1

    :goto_3
    goto :goto_2
.end method
