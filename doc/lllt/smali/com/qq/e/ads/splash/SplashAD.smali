.class public final Lcom/qq/e/ads/splash/SplashAD;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qq/e/ads/splash/SplashAD$ADListenerAdapter;
    }
.end annotation


# instance fields
.field private volatile a:Lcom/qq/e/comm/pi/NSPVI;

.field private volatile b:Landroid/view/ViewGroup;

.field private volatile c:Lcom/qq/e/ads/splash/SplashADListener;

.field private volatile d:Lcom/qq/e/comm/constants/LoadAdParams;

.field private volatile e:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/qq/e/ads/splash/SplashAD;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;ILjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;ILjava/util/Map;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qq/e/ads/splash/SplashAD;->e:Z

    iput-object p5, p0, Lcom/qq/e/ads/splash/SplashAD;->c:Lcom/qq/e/ads/splash/SplashADListener;

    invoke-static {p3}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {p4}, Lcom/qq/e/comm/util/StringUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_8

    if-nez p1, :cond_0

    goto/16 :goto_3

    :cond_0
    invoke-static {p1}, Lcom/qq/e/comm/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string p1, "Required Activity/Service/Permission Not Declared in AndroidManifest.xml"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/16 p1, 0xfa2

    :goto_0
    invoke-static {p5, p1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashADListener;I)V

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object v1

    invoke-virtual {v1, p1, p3}, Lcom/qq/e/comm/managers/GDTADManager;->initWith(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string p1, "Fail to Init GDT AD SDK, report logcat info filter by gdt_ad_mob"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const p1, 0x30da5

    invoke-static {p5, p1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashADListener;I)V

    goto/16 :goto_2

    :cond_2
    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/comm/managers/GDTADManager;->getPM()Lcom/qq/e/comm/managers/plugin/PM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qq/e/comm/managers/plugin/PM;->getPOFactory()Lcom/qq/e/comm/pi/POFactory;

    move-result-object v1

    invoke-interface {v1, p1, p3, p4}, Lcom/qq/e/comm/pi/POFactory;->getNativeSplashAdView(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/qq/e/comm/pi/NSPVI;

    move-result-object p1

    iput-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    if-eqz p1, :cond_6

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->d:Lcom/qq/e/comm/constants/LoadAdParams;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    iget-object p3, p0, Lcom/qq/e/ads/splash/SplashAD;->d:Lcom/qq/e/comm/constants/LoadAdParams;

    invoke-interface {p1, p3}, Lcom/qq/e/comm/pi/NSPVI;->setLoadAdParams(Lcom/qq/e/comm/constants/LoadAdParams;)V

    :cond_3
    if-eqz p7, :cond_4

    invoke-interface {p7}, Ljava/util/Map;->size()I

    move-result p1
    :try_end_0
    .catch Lcom/qq/e/comm/managers/plugin/c; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez p1, :cond_4

    :try_start_1
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1, p7}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-static {}, Lcom/qq/e/comm/managers/GDTADManager;->getInstance()Lcom/qq/e/comm/managers/GDTADManager;

    move-result-object p3

    invoke-virtual {p3}, Lcom/qq/e/comm/managers/GDTADManager;->getSM()Lcom/qq/e/comm/managers/setting/SM;

    move-result-object p3

    const-string p7, "ad_tags"

    invoke-virtual {p3, p7, p1, p4}, Lcom/qq/e/comm/managers/setting/SM;->setDEVCodeSetting(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/qq/e/comm/managers/plugin/c; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_2
    const-string p3, "SplashAD#setTag Exception"

    invoke-static {p3}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {p1, p6}, Lcom/qq/e/comm/pi/NSPVI;->setFetchDelay(I)V

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    new-instance p3, Lcom/qq/e/ads/splash/SplashAD$ADListenerAdapter;

    invoke-direct {p3, p0, v0}, Lcom/qq/e/ads/splash/SplashAD$ADListenerAdapter;-><init>(Lcom/qq/e/ads/splash/SplashAD;B)V

    invoke-interface {p1, p3}, Lcom/qq/e/comm/pi/NSPVI;->setAdListener(Lcom/qq/e/comm/adevent/ADListener;)V

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {p1, p2}, Lcom/qq/e/comm/pi/NSPVI;->setSkipView(Landroid/view/View;)V

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->b:Landroid/view/ViewGroup;

    if-eqz p1, :cond_5

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->b:Landroid/view/ViewGroup;

    invoke-virtual {p0, p1}, Lcom/qq/e/ads/splash/SplashAD;->fetchAndShowIn(Landroid/view/ViewGroup;)V

    :cond_5
    iget-boolean p1, p0, Lcom/qq/e/ads/splash/SplashAD;->e:Z

    if-eqz p1, :cond_7

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {p1}, Lcom/qq/e/comm/pi/NSPVI;->preload()V

    iput-boolean v0, p0, Lcom/qq/e/ads/splash/SplashAD;->e:Z

    goto :goto_2

    :cond_6
    const-string p1, "SplashAdView created by factory return null"

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const p1, 0x30da7

    invoke-static {p5, p1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashADListener;I)V
    :try_end_2
    .catch Lcom/qq/e/comm/managers/plugin/c; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_7
    return-void

    :catchall_0
    move-exception p1

    const-string p2, "Unknown Exception"

    invoke-static {p2, p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0x25d

    invoke-static {p5, p1}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashADListener;I)V

    :goto_2
    return-void

    :catch_1
    move-exception p1

    const-string p2, "Fail to init splash plugin"

    invoke-static {p2, p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const p1, 0x30da6

    goto/16 :goto_0

    :cond_8
    :goto_3
    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    aput-object p3, p2, v0

    const/4 p3, 0x1

    aput-object p4, p2, p3

    const/4 p3, 0x2

    aput-object p1, p2, p3

    const-string p1, "SplashAD Constructor params error, appid=%s,posId=%s,context=%s"

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    const/16 p1, 0x7d1

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;)V
    .locals 6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/qq/e/ads/splash/SplashAD;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V
    .locals 7

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/qq/e/ads/splash/SplashAD;-><init>(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Lcom/qq/e/ads/splash/SplashADListener;I)V

    return-void
.end method

.method static synthetic a(Lcom/qq/e/ads/splash/SplashAD;)Lcom/qq/e/ads/splash/SplashADListener;
    .locals 0

    iget-object p0, p0, Lcom/qq/e/ads/splash/SplashAD;->c:Lcom/qq/e/ads/splash/SplashADListener;

    return-object p0
.end method

.method private static a(Lcom/qq/e/ads/splash/SplashADListener;I)V
    .locals 0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/qq/e/comm/a;->a(I)Lcom/qq/e/comm/util/AdError;

    move-result-object p1

    invoke-interface {p0, p1}, Lcom/qq/e/ads/splash/SplashADListener;->onNoAD(Lcom/qq/e/comm/util/AdError;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final fetchAndShowIn(Landroid/view/ViewGroup;)V
    .locals 1

    if-nez p1, :cond_0

    const-string p1, "SplashAD fetchAndShowIn params null "

    invoke-static {p1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->c:Lcom/qq/e/ads/splash/SplashADListener;

    const/16 v0, 0x7d1

    invoke-static {p1, v0}, Lcom/qq/e/ads/splash/SplashAD;->a(Lcom/qq/e/ads/splash/SplashADListener;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/NSPVI;->fetchAndShowIn(Landroid/view/ViewGroup;)V

    return-void

    :cond_1
    iput-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->b:Landroid/view/ViewGroup;

    return-void
.end method

.method public final getExt()Ljava/util/Map;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    sget-object v0, Lcom/qq/e/comm/pi/NSPVI;->ext:Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    const-string v1, "splash ad can not get extra"

    invoke-static {v1}, Lcom/qq/e/comm/util/GDTLogger;->e(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public final preLoad()V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {v0}, Lcom/qq/e/comm/pi/NSPVI;->preload()V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qq/e/ads/splash/SplashAD;->e:Z

    return-void
.end method

.method public final setLoadAdParams(Lcom/qq/e/comm/constants/LoadAdParams;)V
    .locals 1

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qq/e/ads/splash/SplashAD;->a:Lcom/qq/e/comm/pi/NSPVI;

    invoke-interface {v0, p1}, Lcom/qq/e/comm/pi/NSPVI;->setLoadAdParams(Lcom/qq/e/comm/constants/LoadAdParams;)V

    return-void

    :cond_0
    iput-object p1, p0, Lcom/qq/e/ads/splash/SplashAD;->d:Lcom/qq/e/comm/constants/LoadAdParams;

    return-void
.end method
