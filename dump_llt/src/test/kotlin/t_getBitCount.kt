import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.experimental.and

@RunWith(JUnit4::class)
class TGetBitCount {
    @Test
    fun t_getBitCount() {
        var t1:Int = 0b0001
        println(t1 and 0xff)
        println((t1 and 0xff).shr(0).and(1))
        println(getBitCount(t1))
        assert(getBitCount(t1) == 1)

        t1 = 0b1001
        assert(getBitCount(t1) == 2)

        t1 = 0b0000
        assert(getBitCount(t1) == 0)

        t1 = 0b11111
        assert(getBitCount(t1) == 5)

        t1 = 0b111111
        assert(getBitCount(t1) == 6)
    }

    /**
     * 得到bit位0-5位上1的个数
     */
    fun getBitCount(i: Int): Int {
        val p1 = i and 0xff

        var count = 0
        (0..5).map {
            if (p1.shr(it).and(1) == 1) {
                count ++
            }
        }
        return count
    }

    @Test
    fun bitToInt() {
        val ba  = ByteArray(1) {
            -1
        }
        val b = ba[0].toInt() and 0xFF
        println(b)
    }
}
