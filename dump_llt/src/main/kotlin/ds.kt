import bean.LLTData
import comm.DsTrain
import comm.TrainInfo
import dump.DsStationIndex
import dump.DsTrainIndex

class Ds {
    val trainDsList: MutableList<DsTrain> = mutableListOf()
    val dsTrainIndex: DsTrainIndex = DsTrainIndex().apply { dump() }
    val stationIndex = DsStationIndex().apply { dump() }

    init {
        (0..19)
            .map { DsTrain("$ROOT/t${it}.dat").apply { dump() } }
            .map { trainDsList.add(it) }
    }

    fun getTrainId(trainName: String): Int {
        val trainNameTrim = trainName.trim()
        return dsTrainIndex.data.indexOf(trainNameTrim)
    }

    fun getTrainInfo(trainName: String): TrainInfo? {
        val trainId = getTrainId(trainName)
        if (trainId < 0) return null
        return getTrainInfo(trainId)
    }

    fun getTrainInfo(trainIndex: Int): TrainInfo {
        val fileIndex = (trainIndex + 1) % 20
        val trainDs = trainDsList[fileIndex]
        val unit = trainDs.getUnit(trainIndex, true)
        checkNotNull(unit) {"get trainInfo unit is empty"}
        return TrainInfo(LLTData(unit))
    }

    fun getStationName(index: Int): String {
        return this.stationIndex.data[index]
    }

}

val ds = Ds()