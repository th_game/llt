package dump

import EXTRA_PATH

class ExtraData {
    var trainIdx = 0
    var nQiYe = 0
    var mStations = mutableMapOf<Int, JPK>()

    class JPK {
        var station: Int = 0
        var units = mutableListOf<Unit>()

        class Unit {
            var start: Int = 0
            var end: Int = 0
            var jpk: String = ""

            override fun toString(): String {
                return "Unit(start=$start, end=$end, jpk='$jpk')"
            }
        }

        override fun toString(): String {
            return "JPK(station=$station, units=$units)"
        }

    }

    override fun toString(): String {
        return "dump.ExtraData(trainIdx=$trainIdx, nQiYe=$nQiYe, mStations=$mStations)"
    }
}

class DumpExtra: Dump(EXTRA_PATH) {
    val mQiYe: MutableList<String>  = mutableListOf()
    var mExtraMap = mutableMapOf<Int, ExtraData>()
    var mFuXingHaoMap = mutableMapOf<Int, Int>()

    override fun dump() {
        mQiYe.addAll(getStringList0x8())

        dumpExtraData(readShort0x80())
        dumpFuXingHao()
    }

    private fun dumpExtraData(extraDataSize: Int) {
        0.until(extraDataSize).map {
            ExtraData().apply {
                trainIdx = readShort0x80()
                nQiYe = stream.read()

                val stationSize = stream.read()
                if (stationSize > 0) {
                    0.until(stationSize).map {
                        val jpk = ExtraData.JPK()
                        jpk.station = readShort0x80()

                        val unitSize = stream.read()

                        0.until(unitSize).map {
                            val unit = ExtraData.JPK.Unit()
                            unit.start = readInt4()
                            unit.end = readInt4()
                            unit.jpk = readOneByteLenString()
                            jpk.units.add(unit)
                        }

                        this.mStations[jpk.station] = jpk
                    }
                }
                mExtraMap[trainIdx] = this
            }
        }
    }

    private fun dumpFuXingHao() {
        val size = readShort0x80()
        0.until(size).map {
            mFuXingHaoMap[readShort0x80()] = 0
        }
    }

    override fun toString(): String {
        return "dump.DumpExtra(mQiYe=$mQiYe, mExtraMap=$mExtraMap)"
    }

}