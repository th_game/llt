package dump

import ROOT
import comm.RandomAccessDataInputStream
import java.io.File
import java.nio.charset.Charset

abstract class Dump(var path: String) {
    var stream: RandomAccessDataInputStream
    var size: Int

    init {
        val file = File(path)
        val bytes = file.readBytes()
        stream = RandomAccessDataInputStream(bytes)
        this.size = bytes.size
    }

    abstract fun dump()

    fun getUnitList(p2: Boolean): List<ByteArray> {
        return listOf()
    }

    fun getUnits(hasHead: Boolean): Map<Int, ByteArray> {
        val rst = mutableMapOf<Int, ByteArray>()

        while (!this.isEnd()) {
            val trainIndex = readShort0x80()
            val dataBegin = this.stream.pos
            if (hasHead) {
                this.stream.skip(0xc)
            }

            while (!this.isEnd()) {
                val b = this.stream.read()
                if (b == 0x80) {
                    stream.unread()
                    break
                } else {
                    if (hasHead) {
                        stream.skip(6)
                    }
                }
            }

            val data = ByteArray(stream.pos - dataBegin)
            System.arraycopy(this.stream.bytes, dataBegin, data, 0, data.size)
            rst[trainIndex] = data
            stream.skip(1)
        }
        return rst
    }

    fun getUnit(index: Int, hasHead: Boolean): ByteArray? {
        this.stream.pos = 0
        while (!this.isEnd()) {
            val trainIndex = readShort0x80()
            val dataBegin = this.stream.pos
            if (hasHead) {
                this.stream.skip(0xc)
            }

            while (!this.isEnd()) {
                val b = this.stream.read()
                if (b == 0x80) {
                    stream.unread()
                    break
                } else {
                    if (hasHead) {
                        stream.skip(6)
                    }
                }
            }

            if (trainIndex == index) {
                val rst = ByteArray(stream.pos - dataBegin)
                System.arraycopy(this.stream.bytes, dataBegin, rst, 0, rst.size)
                return rst
            }
            stream.skip(1)
        }
        return null
    }

    fun isEnd(): Boolean {
        return this.stream.pos == this.size
    }

    fun readByteArray(size: Int): ByteArray {
        val rst = ByteArray(size)
        stream.read(rst, 0, size)
        return rst
    }

    /**
     * 得到bit位0-5位上1的个数
     */
    fun getBitCount(i: Int): Int {
        val p1 = i and 0xff

        var count = 0
        (0..5).map {
            if (p1.shr(it).and(1) == 1) {
                count ++
            }
        }
        return count
    }

    /**
     * byte0 << 24 + byte1 << 16 + byte2 << 8 + byte3
     */
    fun readIntBigEndian(): Int {
        val byte0 = stream.read()
        val byte1 = stream.read()
        val byte2 = stream.read()
        val byte3 = stream.read()
        return byte0.shl(24) + byte1.shl(16) + byte2.shl(8) + byte3
    }

    /**
     * byte3 * 0x200000 + byte2 * 0x4000 + byte1 * 0x80 + byte0
     */
    fun readInt4(): Int {
        val byte0 = stream.read()
        val byte1 = stream.read()
        val byte2 = stream.read()
        val byte3 = stream.read()

        return byte3 * 0x200000 + byte2 * 0x4000 + byte1 * 0x80 + byte0
    }

    // 很奇怪是0x80 而不是0x100
    fun readShort0x80(): Int {
        val high = stream.read()
        val low = stream.read()
        return high * 0x80 + low
    }

    fun byteToInt(byte: Byte): Int {
        return byte.toInt() and 0xff
    }

    fun getStringList0x8(): List<String> {
        val size = readShort0x80()
        return 0.until(size).map {
            readOneByteLenString()
        }
            .map {
                it
            }
            .toList()
    }

    /**
     * first byte: is string len
     * then: new String(bytesOfLen(len), "UTF-8")
     */
    fun readOneByteLenString(): String {
        val len = stream.read()
        val bytes = ByteArray(len)
        stream.read(bytes, 0, len)
        return String(bytes, Charset.forName("UTF-8"))
    }

    fun getStringList(): List<String> {
        val size = stream.readUnsignedShort()
        return 0.until(size).map {
                stream.readUTF()
            }
            .map {
                it
            }
        .toList()
    }

}

class Dumps {
    val dwp = DumpDWP()
    val extra = DumpExtra()
    val jlb = DumpJLB()
    // priceData
    val pList = mutableListOf<DumpP>()
    // pk price
    val pkList = mutableListOf<DumpPK>()

    // todos
    val rwp = DumpRwp()
    // s0 - xw


    init {
        (0..4)
            .map {
                "$ROOT/p${it}.dat"
            }
            .map {
                DumpP(it)
            }
            .map {
                pList.add(it)
            }

        (0..9)
            .map {
                "$ROOT/pk${it}.dat"
            }
            .map {
                DumpPK(it)
            }
            .map {
                pkList.add(it)
            }
    }
}
