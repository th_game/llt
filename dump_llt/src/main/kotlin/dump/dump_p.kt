package dump

open class DumpP(path: String): Dump(path){
    val data = mutableMapOf<TwoIndex, ByteArray>()

    override fun dump() {
        while (stream.pos < this.size) {
            val x = readShort0x80()
            val y = readShort0x80()

            val size = stream.read() * 0x6
            stream.unread()

            val bytes = ByteArray(size)
            stream.read(bytes, 0, size)

            this.data[TwoIndex(x, y)] = bytes

            if (stream.pos < this.size) {
                stream.unread()
            }
        }
    }

    override fun toString(): String {
        return "dump.DumpP(data=$data)"
    }


}

data class TwoIndex(val x: Int, val y: Int) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TwoIndex

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        return result
    }

    override fun toString(): String {
        return "[x=$x, y=$y]"
    }

}
