package dump

import SI_PATH
import TRAIN_PATH

open class IndexDump(path: String): Dump(path) {
    val data = mutableListOf<String>()

    override fun dump() {
        this.stream.pos = 0
        this.data.addAll(getStringList())
    }
}

class DsTrainIndex: IndexDump(TRAIN_PATH)

class DsStationIndex: IndexDump(SI_PATH)
