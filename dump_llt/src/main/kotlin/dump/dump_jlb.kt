package dump

import JLB_PATH

class DumpJLB: Dump(JLB_PATH) {
    val foodCoachesList = mutableListOf<FoodCoach>()
    val jlbList = mutableListOf<JLB>()

    override fun dump() {
        dumpJlbList()
        dumpFoodCoachesList()
    }

    private fun dumpFoodCoachesList() {
        var size = readIntBigEndian()
        0.until(size)
            .map {
                FoodCoach().apply { dump(this@DumpJLB) }
            }
            .map { this.foodCoachesList.add(it) }
    }

    private fun dumpJlbList() {
        val size = readIntBigEndian()
        0.until(size).map {
            val jlb = JLB().apply {
                start = readIntBigEndian()
                end = readIntBigEndian()
                rule1 = readIntBigEndian()
                rule2 = readIntBigEndian()
                qiye = stream.read()
                baizu = readOneByteLenString()
                rawTrains = readOneByteLenString()
            }
            jlbList.add(jlb)
        }
    }

    class FoodCoach {
        var index = 0
        var start = 0
        var end = 0
        var coach = ""

        fun dump(dump: Dump) {
            index = dump.readIntBigEndian()
            start = dump.readIntBigEndian()
            end = dump.readIntBigEndian()
            coach = dump.readOneByteLenString()
        }

        override fun toString(): String {
            return "FoodCoach(index=$index, start=$start, end=$end, coach='$coach')"
        }

    }

    class JLB {
        var start: Int = 0
        var end: Int = 0
        var rule1: Int = 0
        var rule2: Int = 0
        var qiye: Int = 0
        var baizu: String = ""
        var rawTrains: String = ""

        override fun toString(): String {
            return "JLB(start=$start, end=$end, rule1=$rule1, rule2=$rule2, qiye=$qiye, baizu='$baizu', rawTrains='$rawTrains')"
        }
    }
}