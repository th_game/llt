package dump

open class DumpPK(path: String): Dump(path){
    val data = mutableMapOf<TwoIndex, ByteArray>()

    override fun dump() {
        while (stream.pos < this.size) {
            val x = readShort0x80()
            val y = readShort0x80()

            val v2 = stream.pos
            val v5 = stream.read()
            var v7 = stream.pos

            0.until(v5).map {
                readShort0x80()
                var v8 = stream.read()
                v8 = getBitCount(v8) * 2
                stream.skip(v8.toLong())
            }
            v7 -= v2

            val bytes = readByteArray(v7)
            this.data[TwoIndex(x, y)] = bytes
        }
    }

    override fun toString(): String {
        return "dump.DumpPK(data=$data)"
    }


}

