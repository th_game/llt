package bean

import comm.RandomAccessDataInputStream

class LLTData(val bytes: ByteArray) {
    val stream: RandomAccessDataInputStream = RandomAccessDataInputStream(bytes)

    val size: Int
    get() {
        return bytes.size
    }

    fun skip(size: Long = 1) {
        this.stream.skip(size)
    }

    fun unread(size: Int = 1) {
        this.stream.unread(size)
    }

    fun get(index: Int): Int {
        return this.bytes[index].toInt() and 0xFF
    }

    fun getShort0x80(index: Int): Int {
        val high = bytes[index]
        val low = bytes[index + 1]
        return high * 0x80 + low
    }

    // 很奇怪是0x80 而不是0x100
    fun readShort0x80(): Int {
        val high = stream.read()
        val low = stream.read()
        return high * 0x80 + low
    }

    fun setPos(pos: Int) {
        stream.pos = pos
    }

    fun getPos(pos: Int) {
        stream.pos = pos
    }

    fun hasEnd(): Boolean {
        return stream.pos > size - 1
    }

}