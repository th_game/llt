package comm

import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.lang.IllegalStateException

class RandomAccessDataInputStream(val bytes: ByteArray): DataInputStream(RandomAccessStream(bytes)) {
    private var byteStream: RandomAccessStream = `in` as RandomAccessStream

    var pos: Int
        get() {
            return byteStream.getPos()
        }
        set(value) {
            byteStream.setPos(value)
        }

    fun unread(size: Int = 1) {
        check(pos >= size) { "can't unread $size bytes because only $pos can unread" }
        pos -= size
    }


    class RandomAccessStream(bytes: ByteArray): ByteArrayInputStream(bytes) {
        fun setPos(pos: Int) {
            this.pos = pos
        }

        fun getPos(): Int {
            return this.pos
        }
    }
}