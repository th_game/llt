package comm

import Ds
import bean.LLTData
import dump.Dump

class TrainInfo(val lltData: LLTData) {
    // why read as beginStation again?
    val fromStation: Int = lltData.getShort0x80(0xc)

    // why read as endStation again?
    val toStation: Int = lltData.getShort0x80(lltData.size - 0x7)

    val type = lltData.get(0x0)

    val stations: MutableList<Node> = mutableListOf()

    init {
        lltData.setPos(12)
        while (!lltData.hasEnd()) {
            val node = Node()
            node.nameIndex = lltData.readShort0x80()
            lltData.skip(3)
            node.mileage = lltData.readShort0x80()
            this.stations.add(node)
        }
    }

    fun initStationName(ds: Ds) {
        for (station in this.stations) {
            station.name = ds.getStationName(station.nameIndex)
        }
    }

    // 途径站点
    class Node {
        var nameIndex: Int = 0

        var name: String = ""
        /**
         * 里程
         */
        var mileage: Int = 0
    }

}


class DsTrain(path: String): Dump(path) {
    val trainInfo = mutableMapOf<Int, TrainInfo>()

    override fun dump() {
        for (unit in getUnits(true)) {
            trainInfo[unit.key] = TrainInfo(LLTData(unit.value))
        }
    }

}
